import 'package:flutter/material.dart';
import 'package:product_layout_app/common/CustomAppBar.dart';
import 'package:product_layout_app/servicecheckup/ManageService.dart';

import '../freeday/freeday_manage.dart';
import '../manager_khoa/test_list_khoa.dart';
import '../manageroom/ManageRoom.dart';
import '../managestaff/ManageStaff.dart';
import '../timeSchedule/createschedule.dart';
import '../vatTu/ql_dvt.dart';

class DetailSpinner extends StatefulWidget {
  //id nhận được từ bên container 1
  final String value1;

  DetailSpinner({super.key, required this.value1});

  @override
  State<DetailSpinner> createState() => _DetailSpinnerState();
}

class _DetailSpinnerState extends State<DetailSpinner> {
  final List<String> listPay = ['Đóng phí 1', 'Đóng phí 2', 'Đóng phí 3'];
  final List<String> listRegistry = ['Kênh 1', 'Kênh 2', 'Kênh 3'];
  final List<String> listState = ['State 1', 'State 2', 'State3'];
  final List<String> listStatus = ['Status 1', 'Status 2', 'Status 3'];
  final List<String> listDanhMuc = [
    'Quản lý khoa',
    'Quản lý phòng',
    'Quản lý nhân viên',
    'Dịch vụ khám',
    'Tạo thời gian khám bệnh',
    'Dược vật tư',
    'Ngày nghỉ'
  ];
  //list trung gian
  final List<String> list = [];

  // late String result = 'Tất cả';
  //map lưu kết quả lựa chọn
  Map<String, String> result = {};

  @override
  Widget build(BuildContext context) {
    print('Giá trị id bên listDrop nhận được là: ${widget.value1}');
    if (widget.value1 == 'idPay') {
      listPay.map((e) => list.add(e)).toList();
    } else if (widget.value1 == 'idRegistry') {
      listRegistry.map((e) => list.add(e)).toList();
    } else if (widget.value1 == 'idState') {
      listState.map((e) => list.add(e)).toList();
    } else if (widget.value1 == 'idStatus') {
      listStatus.map((e) => list.add(e)).toList();
    } else if (widget.value1 == 'danhmuc') {
      listDanhMuc.map((e) => list.add(e)).toString();
    }

    return Scaffold(
        // backgroundColor: Colors.grey,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: CustomAppBar(
              text: "Danh mục",
              check: false,
              onPress: () {
                Navigator.pop(context, [result, 'back']);
              }),
        ),
        body: Container(
          margin: EdgeInsets.only(top: 10),
          child: ListView.builder(
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                    // setState(() {
                    // list.clear();
                    result[widget.value1] = list[index].toString();
                    print(result);
                    print(list[index].toString());
                    print('result[danhmuc] = ${result[widget.value1]}');
                    //  });
                    if (widget.value1 == 'danhmuc') {
                      if (list[index].toString() == "Quản lý khoa") {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => listKhoa()));
                      } else if (list[index].toString() == 'Quản lý phòng') {
                        print('co vao');
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ManageRoom(
                                      checkLoading: false,
                                    )));
                      } else if (list[index].toString() ==
                          'Quản lý nhân viên') {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ManageStaff(
                                      checkLoading: false,
                                    )));
                      } else if (list[index].toString() == 'Dịch vụ khám') {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ManageService(
                                      checkLoading: false,
                                    )));
                      } else if (result['danhmuc'] ==
                          'Tạo thời gian khám bệnh') {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => CreateTimes()));
                      } else if (result['danhmuc'] == 'Dược vật tư') {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => QuanLyVatTu()));
                      } else if (result['danhmuc'] == 'Ngày nghỉ') {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => FreeDayManager()));
                      }
                    } else {
                      Navigator.pop(context, [result, 'click']);
                    }
                  },
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.fromLTRB(8, 0, 8, 0),
                        height: 45,
                        // color: Colors.white,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5),
                            boxShadow: const [
                              BoxShadow(
                                color: Colors.black,
                                offset: Offset(1.0, 1.0),
                                blurRadius: 0.5,
                                spreadRadius: 0.0,
                              ),
                              BoxShadow(
                                color: Colors.white,
                                offset: const Offset(0.0, 0.0),
                                blurRadius: 0.0,
                                spreadRadius: 0.0,
                              ),
                            ]),
                        // margin: EdgeInsets.all(10),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            list[index],
                            style: TextStyle(fontSize: 15),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                    ],
                  ),
                );
              },
              itemCount: list.length),
        ));
  }
}
