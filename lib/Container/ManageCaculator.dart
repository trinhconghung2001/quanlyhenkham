import 'package:flutter/material.dart';
import 'package:product_layout_app/Container/Container1.dart';
import 'package:product_layout_app/common/CustomAppBar.dart';

import '../common/BaseApi.dart';
import '../model/DanhSachLichHen.dart';
import 'DetailManageCaculator.dart';
import 'ItemAppointment.dart';

// ignore: must_be_immutable
class ManageCaculator extends StatefulWidget {
  //điều hướng tĩnh
  // static const routerName = '/manageCaculator';
  ManageCaculator({super.key});

  @override
  State<ManageCaculator> createState() => _ManageCaculatorState();
}

class _ManageCaculatorState extends State<ManageCaculator> {
  ScrollController _scrollController = ScrollController();
  Map<String, String> resultTimKiem = {};
  //khoi tao số lượng hiển thị list ban đầu
  int trang = 1;
  int soDong = 3;
  bool loading = true;
  List<DanhSach> danhsachlichhen = [];
  List<DanhSach> danhsachHienThi = [];
//nhận data trả về từ container1
  void receivedData(Map<String, String> ketqua) {
    setState(() {
      resultTimKiem = ketqua;
      print('ManageCaculator -> Giá trị text đã nhập: ${resultTimKiem}');
      initApi();
    });
  }

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(
      () {
        if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent) {
          load();
        }
      },
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
  //call api

  void initApi() async {
    String apiUrl =
        'https://staging-apidatlichkham.vncare.vn/patient_api/api/QlhkMobile/v1.3.0/business-service/';
    String param =
        'danhSachLichHenKhamCsyt?tuNgay=${resultTimKiem['tuNgay']}&denNgay=${resultTimKiem['denNgay']}&trangThaiDongPhi&kenhDangKy&trangThaiLichHen&trangThaiDenKham&trang=1&soDong=10';
    final ketqua = await BaseApi().get(apiUrl, param);
    DanhSachLichHen kqua = danhSachLichHenFromJson(ketqua);
    setState(() {
      Result ketqua = kqua.result;
      danhsachlichhen = ketqua.danhSach;
      print('Chieu dai list api tra ve: ${danhsachlichhen.length}');
      if (soDong * trang <= danhsachlichhen.length) {
        for (int i = 0; i < soDong * trang; i++) {
          danhsachHienThi.add(danhsachlichhen[i]);
        }
      }
    });
  }

  void load() {
    trang += 1;
    if (loading == false) {
      setState(() {
        loading = true;
      });
    }
    if (soDong * trang <= danhsachlichhen.length) {
      Future.delayed(Duration(seconds: 1), () {
        setState(() {
          for (int i = soDong * (trang - 1); i < soDong * trang; i++) {
            danhsachHienThi.add(danhsachlichhen[i]);
          }
          loading = false;
        });
      });
    } else {
      Future.delayed(Duration(seconds: 1), () {
        setState(() {
          for (int i = soDong * (trang - 1); i < danhsachlichhen.length; i++) {
            danhsachHienThi.add(danhsachlichhen[i]);
          }
          loading = false;
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final width_screen = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: CustomAppBar(
              text: 'Quản lý lịch hẹn khám CSYT',
              check: false,
              onPress: () {
                Navigator.pop(context);
              }),
        ),
        body: Container(
          width: width_screen,
          child: Padding(
            padding: EdgeInsets.only(top: 8, left: 16, right: 16, bottom: 0),
            child: ListView.builder(
                controller: _scrollController,
                itemCount: 3,
                itemBuilder: (BuildContext context, int index) {
                  if (index == 0) {
                    return Card(
                        elevation: 1,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8),
                            side: BorderSide(
                              color: Color.fromARGB(255, 178, 200, 219),
                            )),
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            color: Color.fromARGB(255, 178, 200, 219),
                          ),
                          width: MediaQuery.of(context).size.width,
                          child: Contain1(
                            sentData: receivedData,
                          ),
                        ));
                  } else if (index == 1) {
                    print('trang $trang');
                    print('soDong $soDong');
                    return Container(
                        child: Column(
                      children: [
                        ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: danhsachHienThi.length,
                          itemBuilder: (BuildContext context, int index) {
                            print('Item thu: $index');
                            return GestureDetector(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          DetailManageCaculator(
                                              idHenKham: danhsachHienThi[index]
                                                  .henKhamId)),
                                );
                              },
                              child: ItemAppointment(
                                trangThaiLichHen:
                                    danhsachHienThi[index].maTrangThaiLichHen,
                                title: danhsachHienThi[index].trangThaiLichHen,
                                valueSate:
                                    danhsachHienThi[index].trangThaiDenKham,
                                valueActor: danhsachHienThi[index].tenBenhNhan,
                                valueLocation:
                                    danhsachHienThi[index].tenPhongKham,
                                valueCause: danhsachHienThi[index].yeuCauKham,
                                valueDate: danhsachHienThi[index].ngayGioHen,
                                trangThaiDenKham:
                                    danhsachHienThi[index].maTrangThaiDenKham,
                              ),
                            );
                            // }
                          },
                        ),
                        SizedBox(
                          height: 8,
                        ),
                      ],
                    ));
                  } else {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Visibility(
                          visible: loading,
                          child: CircularProgressIndicator(),
                        ),
                      ],
                    );
                  }
                }),
          ),
        ));
  }
}
