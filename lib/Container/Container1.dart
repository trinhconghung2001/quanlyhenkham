import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:product_layout_app/Container/DetailSpinner.dart';
import 'package:product_layout_app/common/CustomButton.dart';
import 'package:product_layout_app/common/CustomContainer.dart';
import 'package:product_layout_app/componentWidget/icon/qlhk_icons.dart';

class Contain1 extends StatefulWidget {
  final void Function(Map<String, String> resultTimKiem) sentData;
  const Contain1({
    super.key,
    required this.sentData,
  });

  @override
  State<Contain1> createState() => _Contain1State();
}

class _Contain1State extends State<Contain1> {
  //biến lưu id được chọn
  late String idSelected;
  //biến lưu kqua trả về từ listDrop
  Map<String, String> kqua = {};
  //biến lưu kqua để truyền về manageCaculator
  Map<String, String> datamap = {};
  //biến lưu giá trị các thuộc tính
  String resultPay = '',
      resultRegistry = '',
      resultState = '',
      resultStatus = '';

  //lưu id được chọn
  void setStatus(String id) {
    setState(() {
      idSelected = id;
    });
  }

  DateTimeRange dateTimeRange =
      DateTimeRange(start: DateTime.now(), end: DateTime.now());
  @override
  Widget build(BuildContext context) {
    final width_screen = MediaQuery.of(context).size.width;

    //hàm truyền id sang droplist
    void tranfer() async {
      final result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => DetailSpinner(value1: idSelected)),
      );
      if (result != null && result[1] == 'click') {
        setState(() {
          kqua = result[0];
          print('Kết quả trả về từ dropdown -> Kết quả = $kqua');
          if (kqua['idPay'] != null) {
            resultPay = kqua['idPay']!;
            datamap['trangThaiDongPhi'] = resultPay;
          }
          if (kqua['idRegistry'] != null) {
            resultRegistry = kqua['idRegistry']!;
            datamap['kenhDangKy'] = resultRegistry;
          }
          if (kqua['idState'] != null) {
            resultState = kqua['idState']!;
            datamap['trangThaiLichHen'] = resultState;
          }
          if (kqua['idStatus'] != null) {
            resultStatus = kqua['idStatus']!;
            datamap['trangThaiDenKham'] = resultStatus;
          }
        });
      }
    }

    // final height_screen = MediaQuery.of(context).size.height;
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
      child: Container(
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomContainer(
                containerText: 'Từ ngày - Đến ngày',
                buttonText: '${formatDate(dateTimeRange.start, [
                      dd,
                      '/',
                      mm,
                      '/',
                      yyyy
                    ])} - ${formatDate(dateTimeRange.end, [
                      dd,
                      '/',
                      mm,
                      '/',
                      yyyy
                    ])}',
                buttonIcon: Qlhk.calendar,
                onPressed: () {
                  _show();
                },
              ),
              SizedBox(height: 8),
              Container(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: CustomContainer(
                      containerText: 'Đóng phí',
                      // buttonText: '${kqua['idPay']}',
                      buttonText: resultPay,
                      buttonIcon: Icons.arrow_drop_down_sharp,
                      onPressed: () {
                        setStatus('idPay');
                        tranfer();
                      },
                      // height: 30,
                    ),
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Expanded(
                    flex: 2,
                    child: CustomContainer(
                      containerText: 'Kênh đăng ký',
                      // buttonText: '${kqua['idRegistry']}',
                      buttonText: resultRegistry,
                      buttonIcon: Icons.arrow_drop_down_sharp,
                      onPressed: () {
                        setStatus('idRegistry');
                        tranfer();
                      },
                      // height: 30,
                    ),
                  ),
                ],
              )),
              SizedBox(height: 8),
              Container(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: CustomContainer(
                      containerText: 'Trạng thái',
                      // buttonText: '${kqua['idState']}',
                      buttonText: resultState,
                      buttonIcon: Icons.arrow_drop_down_sharp,
                      onPressed: () {
                        setStatus('idState');
                        tranfer();
                      },
                      // height: 30,
                    ),
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Expanded(
                    flex: 2,
                    child: CustomContainer(
                      containerText: 'Trạng thái đến khám',
                      // buttonText: '${kqua['idStatus']}',
                      buttonText: resultStatus,
                      buttonIcon: Icons.arrow_drop_down_sharp,
                      onPressed: () {
                        setStatus('idStatus');
                        tranfer();
                      },
                      // height: 30,
                    ),
                  ),
                ],
              )),
              SizedBox(height: 16),
              Container(
                width: width_screen,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        flex: 2,
                        child: CustomButton(
                            height: 32,
                            textButton: 'Tìm kiếm',
                            buttonIcon: SvgPicture.asset('assets/search.svg'),
                            onPressed: () {
                              widget.sentData(datamap);
                            }),
                      ),
                      SizedBox(
                        width: 16,
                      ),
                      Expanded(
                        flex: 2,
                        child: CustomButton(
                            height: 32,
                            textButton: 'Thêm mới',
                            buttonIcon: SvgPicture.asset('assets/add.svg'),
                            onPressed: () {}),
                      )
                    ]),
              )
            ],
          )),
    );
  }

  void _show() async {
    final DateTimeRange? result = await showDateRangePicker(
      context: context,
      firstDate: DateTime(2023, 5, 1),
      lastDate: DateTime(2023, 8, 30),
      currentDate: DateTime.now(),
      saveText: 'OK',
      initialDateRange: dateTimeRange,
      locale: const Locale('vi'),
    );
    if (result != null) {
      // Rebuild the UI

      print(result.start.toString());
      print(result.end.toString());
      setState(() {
        dateTimeRange = result;
        datamap['tuNgay'] = '${formatDate(dateTimeRange.start, [
              yyyy,
              mm,
              dd,
            ])}';
        datamap['denNgay'] = '${formatDate(dateTimeRange.end, [yyyy, mm, dd])}';
      });
    }
  }
}
