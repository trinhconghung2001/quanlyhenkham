import 'dart:convert';

import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:http/http.dart' as http;
import 'package:product_layout_app/model/dvt.dart';
import 'package:product_layout_app/model/freeday.dart';
// import 'package:test_item/model/dvt.dart';
// import 'package:test_item/model/freeday.dart';

import '../dialog/fail_api.dart';
import '../model/time.dart';

class BaseAPI {
  Future<dynamic> get(String url) async {
    var uri = Uri.parse(url);
    final response = await http.get(uri);
    if (response.statusCode == 200) {
      final result = utf8.decode(response.bodyBytes);
      return result;
    } else {
      return FailApiPopup(trangthai_api: 'Lỗi: ${response.statusCode}');
    }
  }

  Future<dynamic> addTimeAppointment(String url, Times times) async {
    var uri = Uri.parse(url);
    final bodyRequest = json.encode({
      "timeStartAM": times.timeStartAM.toString(),
      "timeEndAM": times.timeEndAM.toString(),
      "timeStartPM": times.timeStartPM.toString(),
      "timeEndPM": times.timeEndPM.toString(),
      "time": times.time.toString(),
      "amountPatient": times.amountPatient.toString(),
      "note": times.note.toString(),
      "state": times.state!.toInt(),
      "isApply": times.isApply,
      // "hourlist": listItem
    });
    final response = await http.post(uri,
        headers: {"Content-Type": "application/json"}, body: bodyRequest);
    if (response.statusCode == 200) {
      final result = utf8.decode(response.bodyBytes);
      return result;
    } else {
      return FailApiPopup(trangthai_api: 'Lỗi: ${response.statusCode}');
    }
  }

  Future<dynamic> updateStateAppointment(
      String url, String id, Times times) async {
    var uri = Uri.parse(url + id);
    List<Hourlist> listItem = [];
    final result = times.hourlist!;
    for (int i = 0; i < result.length; i++) {
      listItem.add(Hourlist(
          id: result[i].id,
          stt: result[i].stt,
          check: result[i].check,
          time: result[i].time));
    }
    print('-----------------------' + uri.toString());
    final bodyRequest = json.encode({
      "timeStartAM": times.timeStartAM.toString(),
      "timeEndAM": times.timeEndAM.toString(),
      "timeStartPM": times.timeStartPM.toString(),
      "timeEndPM": times.timeEndPM.toString(),
      "time": times.time.toString(),
      "amountPatient": times.amountPatient.toString(),
      "note": times.note.toString(),
      "state": times.state!.toInt(),
      "isApply": times.isApply,
      "id": times.id.toString(),
      "hourlist": listItem
    });
    final response = await http.put(uri,
        headers: {"Content-Type": "application/json"}, body: bodyRequest);
    if (response.statusCode == 200) {
      final result = utf8.decode(response.bodyBytes);
      return result;
    } else {
      return FailApiPopup(trangthai_api: 'Lỗi: ${response.statusCode}');
    }
  }

  Future<dynamic> updateStateSVT(String url, String id, DuocVatTu dvt) async {
    var uri = Uri.parse(url + id);
    print('-----------------------' + uri.toString());
    final bodyRequest = json.encode({
      "ma": dvt.ma,
      "ten": dvt.ten,
      "sdk": dvt.sdk,
      "sqd": dvt.sqd,
      "hcsdk": dvt.hcsdk,
      "hamluong": dvt.hamluong,
      "hoatchat": dvt.hoatchat,
      "duongdung": dvt.duongdung,
      "dvcapphat": dvt.dvcapphat,
      "donggoi": dvt.donggoi,
      "hangsx": dvt.hangsx,
      "nuocsx": dvt.nuocsx,
      "sudung": dvt.sudung
    });
    print(bodyRequest.runtimeType);
    final response = await http.put(uri,
        headers: {"Content-Type": "application/json"}, body: bodyRequest);
    if (response.statusCode == 200) {
      final result = utf8.decode(response.bodyBytes);
      return result;
    } else {
      return FailApiPopup(trangthai_api: 'Lỗi: ${response.statusCode}');
    }
  }

  Future<dynamic> updateStateFreeday(
      String url, String id, FreeDay freeDay) async {
    var uri = Uri.parse(url + id);
    print('-----------------------' + uri.toString());
    final bodyRequest = json.encode({
      "type": freeDay.type,
      "thu": freeDay.thu,
      "fromDay": freeDay.fromDay,
      "toDay": freeDay.toDay,
      "freeMorning": freeDay.freeMorning,
      "freeAfternoon": freeDay.freeAfternoon,
      "freeRemote": freeDay.freeRemote,
      "freeCSYT": freeDay.freeCSYT
    });
    final response = await http.put(uri,
        headers: {"Content-Type": "application/json"}, body: bodyRequest);
    if (response.statusCode == 200) {
      final result = utf8.decode(response.bodyBytes);
      return result;
    } else {
      return FailApiPopup(trangthai_api: 'Lỗi: ${response.statusCode}');
    }
  }

  Future<dynamic> deleteTimeAppointment(String url, String id) async {
    var uri = Uri.parse(url + id);
    final response = await http.delete(uri);
    if (response.statusCode == 200) {
      final result = utf8.decode(response.bodyBytes);
      return result;
    } else {
      return FailApiPopup(trangthai_api: 'Lỗi: ${response.statusCode}');
    }
  }

  Future<dynamic> addDVT(String url, DuocVatTu dvt) async {
    var uri = Uri.parse(url);
    final bodyRequest = json.encode({
      "ma": dvt.ma,
      "ten": dvt.ten,
      "sdk": dvt.sdk,
      "sqd": dvt.sqd,
      "hcsdk": dvt.hcsdk,
      "hamluong": dvt.hamluong,
      "hoatchat": dvt.hoatchat,
      "duongdung": dvt.duongdung,
      "dvcapphat": dvt.dvcapphat,
      "donggoi": dvt.donggoi,
      "hangsx": dvt.hangsx,
      "nuocsx": dvt.nuocsx,
      "sudung": dvt.sudung
    });

    final response = await http.post(uri,
        headers: {"Content-Type": "application/json"}, body: bodyRequest);
    if (response.statusCode == 200) {
      final result = utf8.decode(response.bodyBytes);
      return result;
    } else {
      return FailApiPopup(trangthai_api: 'Lỗi: ${response.statusCode}');
    }
  }

  Future<dynamic> addFreeDay(String url, FreeDay freeDay) async {
    var uri = Uri.parse(url);
    final bodyRequest = json.encode({
      "type": freeDay.type,
      "thu": freeDay.thu,
      "fromDay": freeDay.fromDay,
      "toDay": freeDay.toDay,
      "freeMorning": freeDay.freeMorning,
      "freeAfternoon": freeDay.freeAfternoon,
      "freeRemote": freeDay.freeRemote,
      "freeCSYT": freeDay.freeCSYT
    });

    final response = await http.post(uri,
        headers: {"Content-Type": "application/json"}, body: bodyRequest);
    if (response.statusCode == 200) {
      final result = utf8.decode(response.bodyBytes);
      return result;
    } else {
      return FailApiPopup(trangthai_api: 'Lỗi: ${response.statusCode}');
    }
  }
}
