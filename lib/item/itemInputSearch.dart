import 'package:flutter/material.dart';

class ItemSearchInput extends StatelessWidget {
  const ItemSearchInput(
      {required this.title,
      required this.focusNode,
      required this.textEditingController,
      super.key});
  final String title;
  final FocusNode focusNode;
  final TextEditingController textEditingController;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8)),
      ),
      // color: Colors.red,
      margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(fontSize: 12, color: Colors.black),
          ),
          Container(
            height: 25,
            color: Colors.white,
            child: TextFormField(
              focusNode: focusNode,
              controller: textEditingController,
              style: TextStyle(color: Colors.black, fontSize: 12),
              maxLines: 1,
              textAlign: TextAlign.justify,
              decoration: InputDecoration(
                  // suffixIcon: textEditingController.text.length > 0
                  //     ? GestureDetector(
                  //         onTap: () {},
                  //         child: Icon(
                  //           Icons.cancel_outlined,
                  //           size: 14,
                  //         ),
                  //       )
                  //     : null,
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(width: 0.5, color: Colors.white)),
                  fillColor: Colors.white,
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      borderSide: BorderSide(width: 0.5, color: Colors.white))),
            ),
          )
        ],
      ),
    );
  }
}
