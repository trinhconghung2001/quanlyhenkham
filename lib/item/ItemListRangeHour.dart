import 'package:flutter/material.dart';
import 'package:product_layout_app/item/RowTextView.dart';
// import 'package:test_item/item/RowTextView.dart';

import '../model/time.dart';
import '../timeSchedule/schedule_detail.dart';

class HourManage extends StatelessWidget {
  const HourManage(
      {required this.time,
      required this.onCLickIcon,
      required this.callbackData,
      super.key});
  final Times time;
  final Function(bool) onCLickIcon;
  final Function(String) callbackData;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
      child: GestureDetector(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ScheduleDetail(
                        timeschedule: time,
                      ))).then((value) {
            callbackData(value);
          });
        },
        child: Card(
          color: Colors.white,
          shape: RoundedRectangleBorder(
            side: BorderSide(
                color: time.state == 1 ? Color(0xFF6F9BD4) : Color(0xFF5CBBB8),
                width: 1),
            borderRadius: BorderRadius.circular(10),
          ),
          child: Column(
            children: [
              Container(
                height: 35,
                decoration: BoxDecoration(
                    color:
                        time.state == 1 ? Color(0xFF6F9BD4) : Color(0xFF5CBBB8),
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10))),
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width,
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          time.state == 1
                              ? '  Khám tại cơ sở'
                              : '  Tư vấn từ xa',
                          style: TextStyle(fontSize: 12, color: Colors.white),
                        ),
                        Container(
                          padding: EdgeInsets.only(right: 15),
                          child: GestureDetector(
                            onTap: () {
                              onCLickIcon(!time.isApply!);
                            },
                            child: Icon(
                              time.isApply!
                                  ? Icons.check_box_outlined
                                  : Icons.crop_square_sharp,
                              color: Colors.white,
                              size: 16,
                            ),
                          ),
                        )
                      ]),
                ),
              ),
              Container(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    RowTextView(
                      lable: 'Trạng thái',
                      content: time.isApply! ? 'Đang áp dụng' : 'Chưa áp dụng',
                      flexItemLeft: 1,
                      flexItemRight: 1,
                      color2: time.isApply! ? Colors.blue : Colors.red,
                    ),
                    RowTextView(
                      lable: 'Thời gian khám sáng',
                      content: '${time.timeStartAM}'
                          " - "
                          '${time.timeEndAM}',
                      flexItemLeft: 1,
                      flexItemRight: 1,
                    ),
                    RowTextView(
                        lable: 'Thời gian khám chiều',
                        content: '${time.timeStartPM}'
                            " - "
                            '${time.timeEndPM}',
                        flexItemLeft: 1,
                        flexItemRight: 1),
                    RowTextView(
                        lable: 'Thời gian khám/ BN',
                        content: time.time!,
                        flexItemLeft: 1,
                        flexItemRight: 1),
                    RowTextView(
                        lable: 'Ghi chú',
                        content: time.note!,
                        flexItemLeft: 1,
                        flexItemRight: 1),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}


// class HourManage extends StatefulWidget {
//   const HourManage({required this.time, super.key});

//   final Times time;
//   @override
//   State<HourManage> createState() => _HourManageState();
// }

// class _HourManageState extends State<HourManage> {
//   late Times t = widget.time;
//   late String tieude;
//   late bool isCheck = widget.time.isApply;
//   @override
//   void initState() {
//     super.initState();
//     tieude = widget.time.state == 1 ? '  Khám tại cơ sở' : '  Tư vấn từ xa';
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
//       child: GestureDetector(
//         onTap: () {
//           Navigator.push(
//               context,
//               MaterialPageRoute(
//                   builder: (context) => ScheduleDetail(timeschedule: t)));
//         },
//         child: Card(
//           color: Colors.white,
//           shape: RoundedRectangleBorder(
//             side: BorderSide(
//                 color: widget.time.state == 1
//                     ? Color(0xFF6F9BD4)
//                     : Color(0xFF5CBBB8),
//                 width: 1),
//             borderRadius: BorderRadius.circular(10),
//           ),
//           child: Column(
//             children: [
//               Container(
//                 height: 35,
//                 decoration: BoxDecoration(
//                     color: widget.time.state == 1
//                         ? Color(0xFF6F9BD4)
//                         : Color(0xFF5CBBB8),
//                     borderRadius: const BorderRadius.only(
//                         topLeft: Radius.circular(10),
//                         topRight: Radius.circular(10))),
//                 alignment: Alignment.center,
//                 width: MediaQuery.of(context).size.width,
//                 child: Align(
//                   alignment: Alignment.centerLeft,
//                   child: Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       children: [
//                         Text(
//                           tieude,
//                           style: TextStyle(fontSize: 12, color: Colors.white),
//                         ),
//                         Container(
//                           padding: EdgeInsets.only(right: 15),
//                           child: GestureDetector(
//                             onTap: () {
//                               setState(() {
//                                 isCheck = isCheck ? false : true;
//                                 t.setIsApply(isCheck);
//                               });
//                             },
//                             child: Icon(
//                               isCheck
//                                   ? Icons.check_box_outlined
//                                   : Icons.crop_square_sharp,
//                               color: Colors.white,
//                               size: 16,
//                             ),
//                           ),
//                         )
//                       ]),
//                 ),
//               ),
//               Container(
//                 padding: EdgeInsets.all(8.0),
//                 child: Column(
//                   children: [
//                     RowTextView(
//                       lable: 'Trạng thái',
//                       content: isCheck ? 'Đang áp dụng' : 'Chưa áp dụng',
//                       flexItemLeft: 1,
//                       flexItemRight: 1,
//                       color2: isCheck ? Colors.blue : Colors.red,
//                     ),
//                     RowTextView(
//                       lable: 'Thời gian khám sáng',
//                       content: '${widget.time.timeBeginAM}'
//                           " - "
//                           '${widget.time.timeFinishAM}',
//                       flexItemLeft: 1,
//                       flexItemRight: 1,
//                     ),
//                     RowTextView(
//                         lable: 'Thời gian khám chiều',
//                         content: '${widget.time.timeStartPM}'
//                             " - "
//                             '${widget.time.timeFinishPM}',
//                         flexItemLeft: 1,
//                         flexItemRight: 1),
//                     RowTextView(
//                         lable: 'Thời gian khám/ BN',
//                         content: widget.time.time,
//                         flexItemLeft: 1,
//                         flexItemRight: 1),
//                     RowTextView(
//                         lable: 'Ghi chú',
//                         content: widget.time.note,
//                         flexItemLeft: 1,
//                         flexItemRight: 1),
//                   ],
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
