import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../common/CustomButton.dart';
import '../common/CustomContain.dart';

class ContainerSelectRoom extends StatefulWidget {
  final void Function(Map<String, String> resultTimKiem) dataSend;
  ContainerSelectRoom({
    Key? key,
    required this.dataSend,
  }) : super(key: key);

  @override
  State<ContainerSelectRoom> createState() => _ContainerSelectRoomState();
}

class _ContainerSelectRoomState extends State<ContainerSelectRoom> {
  final FocusNode _focusTenPhong = FocusNode();
  final FocusNode _focusMaPhong = FocusNode();
  final FocusNode _focusTuVan = FocusNode();
  final FocusNode _focusDatLich = FocusNode();

  final TextEditingController textTen = TextEditingController();
  final TextEditingController textMa = TextEditingController();
  final TextEditingController textDatLich = TextEditingController();
  final TextEditingController textTuVan = TextEditingController();

  Map<String, String> resultTimKiem = {};

  @override
  void initState() {
    super.initState();
    _focusTenPhong.addListener(_onFocusChange);
    _focusMaPhong.addListener(_onFocusChange);
    _focusTuVan.addListener(_onFocusChange);
    _focusDatLich.addListener(_onFocusChange);
  }

  @override
  void dispose() {
    _focusMaPhong.dispose();
    _focusTenPhong.dispose();
    _focusTuVan.dispose();
    _focusDatLich.dispose();

    super.dispose();
  }

  void _onFocusChange() {
    setState(() {
      resultTimKiem['textTen'] = textTen.text;
      resultTimKiem['textMa'] = textMa.text;
      resultTimKiem['textTuVan'] = textTuVan.text;
      resultTimKiem['textDatLich'] = textDatLich.text;
    });
  }

  @override
  Widget build(BuildContext context) {
    final width_screen = MediaQuery.of(context).size.width;
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
      child: Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: CustomContain(
                      containerText: 'Tên phòng',
                      focusNode: _focusTenPhong,
                      textEditingController: textTen,
                      onPress: () {
                        textTen.clear();
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(height: 8),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: CustomContain(
                      containerText: 'Mã phòng',
                      focusNode: _focusMaPhong,
                      textEditingController: textMa,
                      onPress: () {
                        textMa.clear();
                      },
                    ),
                  ),
                  Container(
                    width: 16,
                  ),
                  Expanded(
                    flex: 2,
                    child: CustomContain(
                      containerText: 'Giá tư vấn online',
                      focusNode: _focusTuVan,
                      textEditingController: textTuVan,
                      onPress: () {
                        textTuVan.clear();
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(height: 8),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: CustomContain(
                      containerText: 'Giá đặt lịch CSYT',
                      focusNode: _focusDatLich,
                      textEditingController: textDatLich,
                      onPress: () {
                        textDatLich.clear();
                      },
                    ),
                  ),
                  Container(
                    width: 16,
                  ),
                  Expanded(
                    flex: 2,
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: 18,
                            width: width_screen / 2,
                          ),
                          SizedBox(height: 4),
                          CustomButton(
                              height: 32,
                              textButton: 'Tìm kiếm',
                              buttonIcon: SvgPicture.asset('assets/search.svg'),
                              onPressed: () {
                                widget.dataSend(resultTimKiem);
                              }),
                        ]),
                  )
                ],
              ),
            ],
          )),
    );
  }
}
