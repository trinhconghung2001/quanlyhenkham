import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'elevated_button.dart';

class InputSearch extends StatelessWidget {
  const InputSearch(
      {required this.textEditingController,
      required this.onClickSearch,
      super.key});
  final TextEditingController textEditingController;
  final Function() onClickSearch;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 5, 5, 5),
      height: 40,
      child: Row(
        children: [
          Expanded(
            flex: 20,
            child: TextFormField(
              textAlignVertical: TextAlignVertical.center,
              controller: textEditingController,
              style: TextStyle(color: Colors.black, fontSize: 12),
              maxLines: 1,
              textAlign: TextAlign.justify,
              decoration: const InputDecoration(
                  errorStyle: TextStyle(height: 2),
                  hintText: 'Nhập tại đây',
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(width: 0.5, color: Colors.black)),
                  fillColor: Colors.blue,
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      borderSide: BorderSide(color: Colors.grey, width: 0.5))),
            ),
          ),
          Expanded(flex: 1, child: SizedBox()),
          Expanded(
            flex: 5,
            child: CustomElevatedButton(
                color: Color(0xFF6F9BD4),
                colorText: Colors.white,
                icon: Icons.search,
                onClickIcon: () {
                  onClickSearch();
                },
                text: ''),
          ),
        ],
      ),
    );
  }
}

// final _textEditingController = TextEditingController();
// Widget InputSearch = Container(
//   margin: EdgeInsets.fromLTRB(0, 5, 5, 5),
//   height: 40,
//   child: Row(
//     children: [
//       Expanded(
//         flex: 20,
//         child: TextFormField(
//           textAlignVertical: TextAlignVertical.center,
//           controller: _textEditingController,
//           style: TextStyle(color: Colors.black, fontSize: 12),
//           maxLines: 1,
//           textAlign: TextAlign.justify,
//           decoration: const InputDecoration(
//               errorStyle: TextStyle(height: 2),
//               hintText: 'Nhập tại đây',
//               focusedBorder: OutlineInputBorder(
//                   borderSide: BorderSide(width: 0.5, color: Colors.black)),
//               fillColor: Colors.blue,
//               enabledBorder: OutlineInputBorder(
//                   borderRadius: BorderRadius.all(Radius.circular(5)),
//                   borderSide: BorderSide(color: Colors.grey, width: 0.5))),
//         ),
//       ),
//       Expanded(flex: 1, child: SizedBox()),
//       Expanded(
//         flex: 5,
//         child: CustomElevatedButton(
//             color: Color(0xFF6F9BD4),
//             colorText: Colors.white,
//             icon: Icons.search,
//             onClickIcon: () {},
//             text: ''),
//       ),
//     ],
//   ),
// );
