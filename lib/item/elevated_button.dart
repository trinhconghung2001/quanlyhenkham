import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class CustomElevatedButton extends StatelessWidget {
  final String text;
  final Function()? onClickIcon;
  final IconData icon;
  final Color color;
  final Color colorText;
  const CustomElevatedButton(
      {required this.onClickIcon,
      required this.text,
      required this.icon,
      required this.color,
      required this.colorText,
      super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ElevatedButton(
          style: ButtonStyle(
              side: MaterialStateProperty.all(
                const BorderSide(
                  color: Color(0xFF6F9BD4), // Màu viền
                  width: 1.0, // Độ rộng viền
                  style: BorderStyle.solid, // Kiểu viền (dotted, dashed, solid)
                ),
              ),
              alignment: Alignment.center,
              backgroundColor: MaterialStatePropertyAll(color),
              foregroundColor: MaterialStatePropertyAll(colorText),
              shape: MaterialStatePropertyAll(RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)))),
          onPressed: () {
            onClickIcon!();
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(icon),
              Text(text),
            ],
          )),
    );
  }
}
