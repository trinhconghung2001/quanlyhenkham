import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class LineStart extends StatelessWidget {
  const LineStart(
      {required this.isCheckAll, required this.onClickIcon, super.key});
  final bool isCheckAll;
  final Function(bool) onClickIcon;
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Container(
              height: 30,
              decoration: BoxDecoration(
                  color: const Color(0xFF6F9BD4),
                  border: Border.all(color: Colors.grey, width: 0.5)),
              child: Center(
                  child: IconButton(
                onPressed: () {
                  onClickIcon(!isCheckAll);
                },
                icon: Icon(
                  isCheckAll
                      ? Icons.check_box_outlined
                      : Icons.crop_square_outlined,
                  size: 20,
                ),
                color: Colors.white,
              ))),
        ),
        Expanded(
          child: Container(
              height: 30,
              decoration: BoxDecoration(
                  color: const Color(0xFF6F9BD4),
                  border: Border.all(color: Colors.grey, width: 0.5)),
              child: const Center(
                child: Text(
                  'STT',
                  style: TextStyle(
                      fontSize: 12,
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                ),
              )),
        ),
        Expanded(
          child: Container(
              height: 30,
              decoration: BoxDecoration(
                  color: const Color(0xFF6F9BD4),
                  border: Border.all(color: Colors.grey, width: 0.5)),
              child: const Center(
                  child: Text('Thời gian khám',
                      style: TextStyle(
                          fontSize: 12,
                          color: Colors.white,
                          fontWeight: FontWeight.bold)))),
        )
      ],
    );
  }
}


// class LineStart extends StatefulWidget {
//   final bool isCheckAll;
//   final Function(bool) onClickIcon;
//   const LineStart(
//       {required this.isCheckAll, required this.onClickIcon, super.key});

//   @override
//   State<LineStart> createState() => _LineStartState();
// }

// class _LineStartState extends State<LineStart> {
//   // late bool ischeck = widget.isCheckAll;
//   @override
//   Widget build(BuildContext context) {
//     return Row(
//       children: [
//         Expanded(
//           child: Container(
//               height: 30,
//               decoration: BoxDecoration(
//                   color: const Color(0xFF6F9BD4),
//                   border: Border.all(color: Colors.grey, width: 0.5)),
//               child: Center(
//                   child: IconButton(
//                 onPressed: () {
//                   widget.onClickIcon(!widget.isCheckAll);
//                 },
//                 icon: Icon(widget.isCheckAll
//                     ? Icons.check_box_outlined
//                     : Icons.crop_square_outlined, size: 20,),
//                 color: Colors.white,
//               ))),
//         ),
//         Expanded(
//           child: Container(
//               height: 30,
//               decoration: BoxDecoration(
//                   color: const Color(0xFF6F9BD4),
//                   border: Border.all(color: Colors.grey, width: 0.5)),
//               child: const Center(
//                 child: Text(
//                   'STT',
//                   style: TextStyle(
//                       fontSize: 12,
//                       color: Colors.white,
//                       fontWeight: FontWeight.bold),
//                 ),
//               )),
//         ),
//         Expanded(
//           child: Container(
//               height: 30,
//               decoration: BoxDecoration(
//                   color: const Color(0xFF6F9BD4),
//                   border: Border.all(color: Colors.grey, width: 0.5)),
//               child: const Center(
//                   child: Text('Thời gian khám',
//                       style: TextStyle(
//                           fontSize: 12,
//                           color: Colors.white,
//                           fontWeight: FontWeight.bold)))),
//         )
//       ],
//     );
//   }
// }
