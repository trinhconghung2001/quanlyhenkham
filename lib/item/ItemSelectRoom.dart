import 'package:flutter/material.dart';

import '../common/CustomButtonCheckbox.dart';
import '../common/CustomTextValue.dart';

// ignore: must_be_immutable
class ItemSelectRoom extends StatelessWidget {
  final String tenPhong;
  final String maPhong;
  bool ktraTuVan;
  final VoidCallback onClickTuVan;
  final VoidCallback onClickDatLich;
  bool ktraDatLich;
  ItemSelectRoom(
      {Key? key,
      required this.tenPhong,
      required this.maPhong,
      required this.onClickTuVan,
      required this.onClickDatLich,
      required this.ktraTuVan,
      required this.ktraDatLich})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 1,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
          side: BorderSide(color: Color(0xFF6F9BD4))),
      child: Container(
        child: Padding(
          padding: EdgeInsets.only(top: 8, left: 16, right: 16, bottom: 8),
          child: Column(
            children: [
              CustomTextValue(
                key1: 'Tên phòng',
                value1: tenPhong,
              ),
              CustomTextValue(
                key1: 'Mã phòng',
                value1: maPhong,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 60,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Expanded(
                        flex: 2,
                        child: CustomButtonCheckbox(
                            height: 32,
                            textButton: 'Tư vấn online',
                            checkIcon: ktraTuVan,
                            onPressed: onClickTuVan),
                      ),
                      Container(
                        width: 8,
                      ),
                      Expanded(
                        flex: 2,
                        child: CustomButtonCheckbox(
                            height: 32,
                            checkIcon: ktraDatLich,
                            textButton: 'Đặt lịch CSYT',
                            onPressed: onClickDatLich),
                      ),
                    ]),
              )
            ],
          ),
        ),
      ),
    );
  }
}
