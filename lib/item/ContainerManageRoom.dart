import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../common/CustomButton.dart';
import '../common/CustomContain.dart';
import '../manageroom/AddEditRoom.dart';
import '../model/listPhongKham.dart';

class ContainerManageRoom extends StatefulWidget {
  final void Function(Map<String, String> ketqua) dataSend;
  final void Function(ListPhongKham listPhongKham) senDataAdd;

  const ContainerManageRoom(
      {Key? key, required this.dataSend, required this.senDataAdd})
      : super(key: key);

  @override
  State<ContainerManageRoom> createState() => _ContainerManageRoomState();
}

class _ContainerManageRoomState extends State<ContainerManageRoom> {
  final FocusNode _focusMaPhong = FocusNode();
  final FocusNode _focusTenPhong = FocusNode();
  final FocusNode _focusKhoa = FocusNode();
  final FocusNode _focusTrangThai = FocusNode();

  final TextEditingController textTen = TextEditingController();
  final TextEditingController textMa = TextEditingController();
  final TextEditingController textTrangThai = TextEditingController();
  final TextEditingController textKhoa = TextEditingController();

  final Map<String, String> ketqua = {};
  @override
  void initState() {
    super.initState();
    _focusTenPhong.addListener(_onFocusChange);
    _focusMaPhong.addListener(_onFocusChange);
    _focusTrangThai.addListener(_onFocusChange);
    _focusKhoa.addListener(_onFocusChange);
  }

  @override
  void dispose() {
    _focusTenPhong.dispose();
    _focusMaPhong.dispose();
    _focusTrangThai.dispose();
    _focusKhoa.dispose();

    super.dispose();
  }

  void _onFocusChange() {
    setState(() {
      ketqua['textTen'] = textTen.text;
      ketqua['textMa'] = textMa.text;
      ketqua['textKhoa'] = textKhoa.text;
      ketqua['textTrangThai'] = textTrangThai.text;
    });
  }

  @override
  Widget build(BuildContext context) {
    final width_screen = MediaQuery.of(context).size.width;
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
      child: Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: CustomContain(
                      containerText: 'Mã phòng',
                      focusNode: _focusMaPhong,
                      textEditingController: textMa,
                      onPress: () {
                        textMa.clear();
                      },
                    ),
                  ),
                  Container(
                    width: 16,
                  ),
                  Expanded(
                    flex: 2,
                    child: CustomContain(
                      containerText: 'Tên phòng',
                      focusNode: _focusTenPhong,
                      textEditingController: textTen,
                      onPress: () {
                        textTen.clear();
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(height: 8),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: CustomContain(
                      containerText: 'Khoa',
                      focusNode: _focusKhoa,
                      textEditingController: textKhoa,
                      onPress: () {
                        textKhoa.clear();
                      },
                    ),
                  ),
                  Container(
                    width: 16,
                  ),
                  Expanded(
                    flex: 2,
                    child: CustomContain(
                      containerText: 'Trạng thái',
                      focusNode: _focusTrangThai,
                      textEditingController: textTrangThai,
                      onPress: () {
                        textTrangThai.clear();
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(height: 16),
              Container(
                width: width_screen,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Expanded(
                        flex: 2,
                        child: CustomButton(
                            height: 32,
                            textButton: 'Tìm kiếm',
                            buttonIcon: SvgPicture.asset('assets/search.svg'),
                            onPressed: () {
                              widget.dataSend(ketqua);
                            }),
                      ),
                      Container(
                        width: 16,
                      ),
                      Expanded(
                        flex: 2,
                        child: CustomButton(
                            height: 32,
                            textButton: 'Thêm mới',
                            buttonIcon: SvgPicture.asset('assets/add.svg'),
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AddEditRoom(
                                            checkAddEdit: true,
                                            listPhongKham: ListPhongKham(
                                                khoa: '',
                                                khoaNoiTru: '',
                                                maPhong: '',
                                                maPhongBhyt: '',
                                                soPhong: '',
                                                tenPhong: '',
                                                chuyenKhoa: '',
                                                loaiPhong: '',
                                                maDauDoc: '',
                                                ghiChu: '',
                                                qrCode: '',
                                                diaChiPhong: '',
                                                maMau: '',
                                                checkPhongGiaoSu: false,
                                                checkDangKiHen: false,
                                                stt: '',
                                                sttPhong: '',
                                                chuyenKhoaDrop: '',
                                                congKham: '',
                                                checkSuDung: false,
                                                id: '',
                                                datLichCsyt: false,
                                                tuVanOnline: false),
                                          ))).then((value) {
                                if (value[1] == 'add') {
                                  widget.senDataAdd(value[0]);
                                }
                              });
                            }),
                      )
                    ]),
              )
            ],
          )),
    );
  }
}
