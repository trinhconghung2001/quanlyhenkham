import 'package:flutter/material.dart';

class LineTextBorder extends StatefulWidget {
  LineTextBorder(
      {required this.textEditingController,
      required this.title,
      required this.value,
      super.key});
  TextEditingController textEditingController;
  final String title;
  final String value;
  @override
  State<LineTextBorder> createState() => _LineTextBorderState();
}

class _LineTextBorderState extends State<LineTextBorder> {
  // late TextEditingController _textEditingController = TextEditingController(
  //   text: widget.value.length > 15
  //       ? widget.value.substring(0, 15) + "..."
  //       : widget.value,
  // );
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 5,
            child: Container(
                child: Text(widget.title, style: TextStyle(fontSize: 12))),
          ),
          Expanded(
            flex: 5,
            child: Container(
                decoration: BoxDecoration(),
                child: Container(
                  height: 25,
                  // margin: const EdgeInsets.fromLTRB(0, 5, 5, 5),
                  child: TextFormField(
                    controller: widget.textEditingController,
                    style: TextStyle(fontSize: 12),
                    maxLines: 1,
                    textAlignVertical: TextAlignVertical.center,
                    textAlign: TextAlign.left,
                    decoration: const InputDecoration(
                        focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(width: 0.5, color: Colors.black)),
                        fillColor: Colors.blue,
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            borderSide:
                                BorderSide(color: Colors.grey, width: 0.5))),
                  ),
                )),
          ),
        ],
      ),
    );
  }
}
