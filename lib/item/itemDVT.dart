import 'package:flutter/material.dart';
import 'package:product_layout_app/item/RowTextView.dart';
// import 'package:test_item/item/RowTextView.dart';

import '../model/dvt.dart';
import '../vatTu/dvt_detail.dart';

class ItemQLDVT extends StatefulWidget {
  const ItemQLDVT({required this.dvt, required this.callbackData, super.key});
  final DuocVatTu dvt;
  final Function(String) callbackData;
  @override
  State<ItemQLDVT> createState() => _ItemQLDVTState();
}

class _ItemQLDVTState extends State<ItemQLDVT> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
      child: GestureDetector(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ChiTietDuocVatTu(
                        dvt: widget.dvt,
                      ))).then((value) {
            widget.callbackData(value);
          });
        },
        child: Card(
          color: Colors.white,
          shape: RoundedRectangleBorder(
            side: BorderSide(
                color:
                    widget.dvt.sudung! ? Color(0xFF6F9BD4) : Color(0xFFB43939),
                width: 1),
            borderRadius: BorderRadius.circular(10),
          ),
          child: Column(
            children: [
              Container(
                height: 35,
                decoration: BoxDecoration(
                    color: widget.dvt.sudung!
                        ? const Color(0xFF6F9BD4)
                        : const Color(0xFFB43939),
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10))),
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width,
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    widget.dvt.sudung == true ? '  Hiệu lực' : '  Hết hiệu lực',
                    style: TextStyle(fontSize: 12, color: Colors.white),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    RowTextView(
                        lable: 'Mã',
                        content: widget.dvt.ma!,
                        flexItemLeft: 1,
                        flexItemRight: 1),
                    RowTextView(
                        lable: 'Tên',
                        content: widget.dvt.ten!,
                        flexItemLeft: 1,
                        flexItemRight: 1),
                    RowTextView(
                        lable: 'Hoạt chất',
                        content: widget.dvt.hoatchat!,
                        flexItemLeft: 1,
                        flexItemRight: 1),
                    RowTextView(
                        lable: 'Đơn vị cấp phát',
                        content: widget.dvt.dvcapphat!,
                        flexItemLeft: 1,
                        flexItemRight: 1),
                    RowTextView(
                        lable: 'Hàm lượng',
                        content: widget.dvt.hamluong!,
                        flexItemLeft: 1,
                        flexItemRight: 1),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
