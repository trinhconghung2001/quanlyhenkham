import 'package:flutter/material.dart';

var BoxDecorationSearch = BoxDecoration(
  color: Color.fromARGB(255, 181, 208, 244),
  borderRadius: BorderRadius.circular(8),
  boxShadow: [
    BoxShadow(
      color: Colors.grey.withOpacity(0.5),
      spreadRadius: 2,
      blurRadius: 2,
      offset: Offset(0, 1),
    ),
  ],
);
