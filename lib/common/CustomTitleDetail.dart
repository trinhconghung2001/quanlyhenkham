import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomTitleDetail extends StatelessWidget {
  final String keyItem;
  final String valueItem;
  final Icon? icon;
  final bool check;
  final int left;
  final int right;
  final String trangThaiDenKham;

  CustomTitleDetail(
      {super.key,
      required this.keyItem,
      required this.valueItem,
      this.icon,
      this.check = false,
      this.trangThaiDenKham = '',
      this.left = 3,
      this.right = 5});

  @override
  Widget build(BuildContext context) {
    Color colorValue;
    Color colorTitle;
    if (trangThaiDenKham == '2') {
      // textColor = Color(0xFF5CBBB8);
      colorValue = Color(0xFFB43939);
    } else if (trangThaiDenKham == '1') {
      colorValue = Color(0xFF5CBBB8);
    } else {
      colorValue = Color(0xFF535858);
    }

    if (keyItem == 'Số thẻ BHYT (*)') {
      colorTitle = Color(0xFFB43939);
      colorValue = Color(0xFFB43939);
    } else {
      colorTitle = Color(0xFF535858);
    }
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: Row(
        children: [
          Expanded(
              flex: left,
              child: Container(
                  alignment: Alignment.centerLeft,
                  height: 30,
                  child: Text(keyItem,
                      style: TextStyle(fontSize: 14, color: colorTitle)))),
          SizedBox(
            width: 8,
          ),
          Expanded(
              flex: right,
              child: Container(
                alignment: Alignment.centerLeft,
                height: 30,
                child: Text(valueItem,
                    style: valueItem != 'Xác thực thẻ bhyt'
                        ? TextStyle(fontSize: 14, color: colorValue)
                        : TextStyle(
                            decoration: TextDecoration.underline,
                            fontSize: 14,
                            color: Color(0xFF6F9BD4))),
              )),
          Expanded(
            flex: 1,
            child: Container(
              alignment: Alignment.centerRight,
              margin: EdgeInsets.only(right: 8),
              height: 30,
              child: check == true ? icon : null,
            ),
          )
        ],
      ),
    );
  }
}
