import 'package:flutter/material.dart';

class CustomTextValue extends StatelessWidget {
  final String key1;
  final String value1;
  final int left;
  final int right;
  final String trangThaiDenKham;
  CustomTextValue({
    Key? key,
    required this.key1,
    required this.value1,
    this.trangThaiDenKham = '',
    this.left = 2,
    this.right = 3,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Color colorValue;
    if (trangThaiDenKham == '2') {
      // textColor = Color(0xFF5CBBB8);
      colorValue = Color(0xFFB43939);
    } else if (trangThaiDenKham == '1') {
      colorValue = Color(0xFF5CBBB8);
    } else {
      colorValue = Color(0xFF444444);
    }

    return Row(
      children: [
        Expanded(
          child: Container(
            height: 30,
            alignment: Alignment.centerLeft,
            child: Text(key1,
                style: TextStyle(
                  fontSize: 14,
                )),
          ),
          flex: left,
        ),
        SizedBox(
          width: 8,
        ),
        Expanded(
          child: Container(
            height: 30,
            alignment: Alignment.centerLeft,
            child: Text(value1,
                style: TextStyle(
                  fontSize: 14,
                  color: colorValue,
                )),
          ),
          flex: right,
        )
      ],
    );
  }
}
