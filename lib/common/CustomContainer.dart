import 'package:flutter/material.dart';

class CustomContainer extends StatelessWidget {
  final String containerText;
  final String buttonText;
  final IconData buttonIcon;
  final VoidCallback onPressed;
  const CustomContainer(
      {super.key,
      required this.containerText,
      required this.buttonText,
      required this.buttonIcon,
      required this.onPressed,});

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          
          Container(
            height: 18,
            // width: ,
            child: Text(
              containerText,
              style: TextStyle(fontSize: 12, color: Color(0xFFF535858)),
            ),
          ),
          SizedBox(height: 4),
          Container(
              height: 29,
              child: ElevatedButton(
                // ignore: sort_child_properties_last
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      buttonText,
                      style: TextStyle(fontSize: 14),
                    ),
                    Icon(
                      buttonIcon,
                      size: 17,
                      color: Color(0xFF444444),
                    )
                  ],
                ),
                onPressed: onPressed,
                style: ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(Colors.white),
                    foregroundColor:
                        MaterialStatePropertyAll(Color(0xFF444444)),
                    shape: MaterialStatePropertyAll(RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)))),
              ))
        ]);
  }
}
