import 'package:flutter/material.dart';
import 'package:product_layout_app/componentWidget/icon/qlhk_icons.dart';

class CustomContain extends StatelessWidget {
  final String containerText;
  final VoidCallback onPress;
  final FocusNode focusNode;
  final TextEditingController textEditingController;
  CustomContain({
    Key? key,
    required this.containerText,
    required this.onPress,
    required this.textEditingController,
    required this.focusNode,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 18,
            child: Text(
              containerText,
              style: TextStyle(fontSize: 12),
            ),
          ),
          SizedBox(height: 4),
          Container(
              // margin: EdgeInsets.only(right: 5),
              height: 29,
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Colors.grey, width: 0.5),
                  borderRadius: BorderRadius.all(Radius.circular(8))),
              child: TextFormField(
                controller: textEditingController,
                focusNode: focusNode,
                style: TextStyle(
                  fontSize: 14,
                ),
                decoration: InputDecoration(
                    suffixIcon: focusNode.hasFocus == false &&
                            textEditingController.text.isNotEmpty
                        ? GestureDetector(
                            onTap: onPress,
                            child: Icon(
                              Qlhk.cancel,
                              size: 14,
                            ),
                          )
                        : null,
                    border: InputBorder.none,
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(width: 1, color: Colors.grey)),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        borderSide:
                            BorderSide(color: Colors.white, width: 1.5))),
              )),
        ]);
  }
}
