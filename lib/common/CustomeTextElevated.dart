import 'package:flutter/material.dart';

class CustomTextElevated extends StatelessWidget {
  final String title;
  final IconData icon;
  final VoidCallback onpressed;
  final String text;
  final bool checkRangBuoc;
  final int left;
  final int right;
  const CustomTextElevated({
    Key? key,
    required this.onpressed,
    required this.text,
    required this.title,
    this.checkRangBuoc = false,
    this.icon = Icons.arrow_drop_down_sharp,
    this.left = 2,
    this.right = 3,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
          Expanded(
            child: Container(
              height: 30,
              alignment: Alignment.centerLeft,
              child: checkRangBuoc == false
                  ? Text(
                      title,
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    )
                  : Row(
                      children: [
                        Text(
                          title,
                          style: TextStyle(fontSize: 14),
                        ),
                        Text(
                          ' (*)',
                          style: TextStyle(color: Colors.red),
                        )
                      ],
                    ),
            ),
            flex: left,
          ),
          SizedBox(
            width: 8,
          ),
          Expanded(
            flex: right,
            child: GestureDetector(
              onTap: onpressed,
              child: Container(
                  height: 30,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(color: Colors.grey, width: 1.5),
                      borderRadius: BorderRadius.all(Radius.circular(8))),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 12, right: 17),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          text,
                          style: TextStyle(
                            fontSize: 14,
                          ),
                        ),
                        Icon(
                          icon,
                          size: 14,
                          color: Color(0xFF444444),
                        )
                      ],
                    ),
                  )),
            ),
          ),
        ]),
        Container(
          height: 8,
        )
      ],
    );
  }
}
