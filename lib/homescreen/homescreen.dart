import 'package:flutter/material.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:product_layout_app/login.dart';
import 'package:simple_shadow/simple_shadow.dart';
import 'package:badges/badges.dart' as badges;
import '../notifications/notification.dart';
import 'package:product_layout_app/Container/ManageCaculator.dart';


class mainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String token = SharedPrefs().token;

    //print(token);
    // DateTime expirationDate = JwtDecoder.getExpirationDate(token);
    // print(expirationDate);
    Map<String, dynamic> decodedToken = JwtDecoder.decode(token);
    String name = decodedToken["fullName"];
    String id = decodedToken["userID"].toString();
    String hospital = decodedToken["hospitalName"];
    //print(token);
    //bool hasExpired = JwtDecoder.isExpired(token);
    //print(hasExpired);

    double width1 = MediaQuery.of(context).size.width;
    double height1 = MediaQuery.of(context).size.height;

    return Scaffold(
      //backgroundColor: Colors.transparent,
      body: Stack(
        //crossAxisAlignment: CrossAxisAlignment.center,
        //mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Positioned(
            left: 0,
            top: 0,
            bottom: MediaQuery.of(context).size.height * 0.47,
            //bottom: 0,
            right: 0,
            //height: 400,
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.4,
              decoration: BoxDecoration(
                color: Color(0xFF6f9bd4),
              ),
              child: Stack(
                children: [
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 20),
                        Container(
                          padding:
                              EdgeInsets.symmetric(vertical: 0, horizontal: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 10),
                                child: Text(
                                  'Xin chào,',
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w600,
                                    height: 2,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  Navigator.of(context).push(
                                    MaterialPageRoute(
                                        builder: (context) => notifications()),
                                  );
                                },
                                child: Container(
                                  padding: EdgeInsets.only(
                                      bottom: 0, left: 40, right: 0, top: 20),
                                  decoration: BoxDecoration(
                                    color: Color(0xFF6f9bd4),
                                    //color: Colors.white,
                                  ),
                                  child: badges.Badge(
                                    badgeContent: Text(
                                      '3',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 10,
                                      ),
                                    ),
                                    position: badges.BadgePosition.topEnd(
                                        top: -5, end: 15),
                                    badgeStyle: badges.BadgeStyle(
                                      badgeColor: Colors.red,
                                      //padding: EdgeInsets.all(10),
                                    ),
                                    child: Icon(
                                      Icons.notifications,
                                      color: Colors.white,
                                      size: 30,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsetsDirectional.symmetric(
                              horizontal: 15, vertical: 0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                '$name - $id',
                                //'${SharedPrefs().token}',
                                //'Nguyễn Văn Anh - NVA.123',
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                ),
                              ),
                              SizedBox(height: 15),
                              Container(
                                width: width1 * 0.55,
                                child: Text(
                                  //'Bệnh viện bưu điện',
                                  '$hospital',
                                  maxLines: 2,
                                  softWrap: true,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.normal,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    child: Stack(
                      children: [
                        Positioned(
                          child: SvgPicture.asset(
                            'assets/Group1.svg', //khung to ong chim
                            color: Colors.blueGrey,
                            //color: Colors.blue,
                            // width: 200,
                            // height: 200,
                          ),
                          bottom: 20,
                          top: 110,
                          left: 90,
                          right: 0,
                        ),
                        Positioned(
                          child: SimpleShadow(
                            child: SvgPicture.asset(
                              'assets/Vector3.svg',
                              color: Colors.white, //khung to ong nổi
                              //color: Colors.transparent,
                            ),
                            color: Colors.black,
                            opacity: 1,
                            offset: Offset(4, 4), // Default: Offset(2, 2)
                            sigma: 2,
                          ),
                          bottom: 15,
                          top: 110,
                          left: 0,
                          right: 0,
                        ),
                        Positioned(
                          child: SvgPicture.asset(
                            'assets/Group 615527.svg', //6 vật thể trong khung tổ ong
                            color: Colors.white,
                            // width: 200,
                            // height: 200,
                          ),

                          bottom: 26,
                          //top: 100,
                          right: 41,
                        ),
                        Positioned(
                          child: SvgPicture.asset(
                            'assets/Group 15525.svg', // 2 bóng tròn
                            // width: 1200,
                            // height: 400,
                            fit: BoxFit.scaleDown,
                            allowDrawingOutsideViewBox: true,
                            matchTextDirection: true,
                          ),
                          bottom: 10,
                          top: 5,
                          left: -70,
                          right: -50, //done
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).size.height * 0.43,
            //bottom: MediaQuery.of(context).size.height * 0.4,
            bottom: 0,
            left: 0,
            right: 0,
            child: Stack(
              children: [
                Container(
                  width: width1,
                  height: height1 * 0.6,
                  decoration: BoxDecoration(
                    color: Color(0xFF6f9bd4),
                  ),
                ),
                Container(
                  width: width1,
                  height: height1 * 0.6,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(50),
                      topRight: Radius.circular(50),
                    ),
                  ),
                  child: Column(
                    children: [
                      SizedBox(height: 8),
                      Text(
                        'Danh mục quản lý',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w600,
                          color: Color(0xFF535858),
                        ),
                      ),
                      SizedBox(height: 15),
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context) => ManageCaculator()),
                          );
                        },
                        child: Container(
                            child: Column(
                          children: [
                            Stack(
                              children: [
                                Column(
                                  children: [
                                    SizedBox(height: 25),
                                    Container(
                                      width: width1 * 0.9,
                                      height: height1 * 0.14,
                                      padding: EdgeInsetsDirectional.symmetric(
                                        vertical: 10,
                                      ),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(32)),
                                        color: Color(0xFF5cbbb8),
                                      ),
                                    ),
                                  ],
                                ),
                                Positioned(
                                  child: SvgPicture.asset(
                                      'assets/Doctor-Appoinment 1.svg'),
                                  bottom: 0,
                                  left: 0,
                                ),
                                Positioned(
                                  child: Text(
                                    'Quản lý lịch hẹn\n'
                                    'khám tại cơ sở y tế',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.white,
                                    ),
                                  ),
                                  left: 140,
                                  bottom: 27,
                                ),
                                Positioned(
                                  child: IconButton(
                                    onPressed: () {},
                                    icon: Icon(Icons.chevron_right),
                                    color: Colors.white,
                                  ),
                                  left: 282,
                                  bottom: 25,
                                ),
                              ],
                            ),
                          ],
                        )),
                      ),
                      SizedBox(height: 10),
                      Container(
                          child: Column(
                        children: [
                          Stack(
                            children: [
                              Column(
                                children: [
                                  SizedBox(height: 35),
                                  Container(
                                    width: width1 * 0.9,
                                    height: height1 * 0.14,
                                    padding: EdgeInsetsDirectional.symmetric(
                                      vertical: 10,
                                    ),
                                    decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(32)),
                                      color: Color(0xFF6f9bd4),
                                    ),
                                  ),
                                ],
                              ),
                              Positioned(
                                child: SvgPicture.asset(
                                    'assets/Doctor-Consultation 1.svg'),
                                bottom: 0,
                                left: 0,
                              ),
                              Positioned(
                                child: Text(
                                  'Quản lý lịch\n'
                                  'tư vấn online',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 18,
                                    color: Colors.white,
                                  ),
                                ),
                                left: 160,
                                bottom: 25,
                              ),
                              Positioned(
                                child: IconButton(
                                  onPressed: () {},
                                  icon: Icon(Icons.chevron_right),
                                  color: Colors.white,
                                ),
                                left: 282,
                                bottom: 23,
                              ),
                            ],
                          ),
                        ],
                      )),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
