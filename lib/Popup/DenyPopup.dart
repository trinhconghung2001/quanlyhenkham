import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class DenyPopup extends StatefulWidget {
  const DenyPopup({
    super.key,
  });

  @override
  State<DenyPopup> createState() => _DenyPopupState();
}

class _DenyPopupState extends State<DenyPopup> {
  String lyDo = '';
  final FocusNode _focusLyDo = FocusNode();
  final TextEditingController textLyDo = TextEditingController();
  @override
  void initState() {
    super.initState();
    _focusLyDo.addListener(_onFocusChange);
  }

  @override
  void dispose() {
    _focusLyDo.dispose();
  }

  void _onFocusChange() {
    setState(() {
      lyDo = textLyDo.text;
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Align(
        alignment: Alignment.center,
        child: Container(
          width: 300,
          height: 270,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            color: Colors.white,
          ),
          child: Column(children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 10, bottom: 0, right: 5, left: 270),
              child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: SvgPicture.asset('assets/cancel.svg')),
            ),
            Padding(
                padding: EdgeInsets.symmetric(vertical: 7),
                child: SvgPicture.asset('assets/sad.svg')),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5),
              child: Text(
                'BẠN CHẮC CHẮN CHỨ',
                style: TextStyle(
                  color: Color(0xFF444444),
                  fontSize: 20,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5),
              child: Text(
                'Hãy cho tôi biết lý do',
                style: TextStyle(
                  color: Color(0xFF535858),
                  fontSize: 14,
                ),
              ),
            ),
            Padding(
                padding:
                    EdgeInsets.only(top: 10, bottom: 0, left: 15, right: 15),
                child: Column(children: [
                  // TextField(
                  //   decoration: InputDecoration(
                  //     border: OutlineInputBorder(),
                  //     hintText: 'Enter a search term',
                  //   ),
                  // ),
                  Container(
                      height: 35,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(color: Colors.grey, width: 1.5),
                          borderRadius: BorderRadius.all(Radius.circular(8))),
                      child: Material(
                        child: TextFormField(
                          style: TextStyle(
                            fontSize: 14,
                          ),
                          focusNode: _focusLyDo,
                          controller: textLyDo,
                          // validator: (value) {
                          //   if (checkRangBuoc == true && value == '') {
                          //     print('Vui long nhap gia tri');
                          //   }
                          // },
                          decoration: InputDecoration(
                              suffixIcon: _focusLyDo.hasFocus == false &&
                                      textLyDo.text.isNotEmpty
                                  ? GestureDetector(
                                      child: Icon(
                                        Icons.clear,
                                        size: 14,
                                      ),
                                      onTap: () {
                                        textLyDo.clear();
                                      },
                                    )
                                  : null,
                              border: InputBorder.none,
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      width: 0.5, color: Colors.grey)),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8)),
                                  borderSide: BorderSide(
                                      color: Colors.white, width: 0.5))),
                        ),
                      )),
                  SizedBox(
                    height: 10,
                  ),
                  ElevatedButton(
                    // ignore: sort_child_properties_last
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [Text('Gửi')],
                    ),
                    onPressed: () {
                      print('DenyPopup -> Lý do từ chối là: ${textLyDo.text}');
                    },
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStatePropertyAll(Color(0xFF6F9BD4)),
                        foregroundColor: MaterialStatePropertyAll(Colors.white),
                        side: MaterialStatePropertyAll(
                            BorderSide(color: Color(0xFF6F9BD4))),
                        shape: MaterialStatePropertyAll(RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8)))),
                  ),
                ])),
          ]),
        ),
      ),
    );
  }
}
