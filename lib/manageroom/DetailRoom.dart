import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../common/BaseApiNoToken.dart';
import '../common/CustomAppBar.dart';
import '../common/CustomButton.dart';
import '../common/CustomTextCheckbox.dart';
import '../common/CustomTextValue.dart';
import '../model/listPhongKham.dart';
import 'AddEditRoom.dart';

// ignore: must_be_immutable
class DetailRoom extends StatefulWidget {
  ListPhongKham listPhongKham;
  DetailRoom({Key? key, required this.listPhongKham}) : super(key: key);

  @override
  State<DetailRoom> createState() => _DetailRoomState();
}

class _DetailRoomState extends State<DetailRoom> {
  bool checkThayDoiEdit = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50),
        child: CustomAppBar(
          text: 'Chi tiết thông tin phòng',
          check: false,
          onPress: () {
            if (checkThayDoiEdit == false) {
              Navigator.pop(context, [widget.listPhongKham, 'back']);
            } else {
              Navigator.pop(context, [widget.listPhongKham, 'sua']);
            }
          },
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: 16,
          vertical: 8,
        ),
        child: Container(
          // height: MediaQuery.of(context).size.height - 152,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Wrap(children: [
                  Card(
                    elevation: 4,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8),
                        side: BorderSide(color: Colors.white)),
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: 16,
                        vertical: 8,
                      ),
                      child: Column(
                        children: [
                          CustomTextValue(
                              key1: 'Khoa', value1: widget.listPhongKham.khoa),
                          CustomTextValue(
                              key1: 'Khoa nội trú',
                              value1: widget.listPhongKham.khoaNoiTru),
                          CustomTextValue(
                              key1: 'Mã phòng',
                              value1: widget.listPhongKham.maPhong),
                          CustomTextValue(
                              key1: 'Mã phòng bhyt',
                              value1: widget.listPhongKham.maPhongBhyt),
                          CustomTextValue(
                              key1: 'Số phòng',
                              value1: widget.listPhongKham.soPhong),
                          CustomTextValue(
                              key1: 'Tên phòng',
                              value1: widget.listPhongKham.tenPhong),
                          CustomTextValue(
                              key1: 'Chuyên khoa',
                              value1: widget.listPhongKham.chuyenKhoa),
                          CustomTextValue(
                              key1: 'Loại phòng',
                              value1: widget.listPhongKham.loaiPhong),
                          CustomTextValue(
                              key1: 'Mã đầu đọc',
                              value1: widget.listPhongKham.maDauDoc),
                          CustomTextValue(
                              key1: 'Ghi chú',
                              value1: widget.listPhongKham.ghiChu),
                          CustomTextValue(
                              key1: 'QR code',
                              value1: widget.listPhongKham.qrCode),
                          CustomTextValue(
                              key1: 'Địa Chỉ Phòng',
                              value1: widget.listPhongKham.diaChiPhong),
                          CustomTextValue(
                              key1: 'Chọn mã màu',
                              value1: widget.listPhongKham.maMau),
                          CustomTextCheckbox(
                            checkIcon: widget.listPhongKham.checkPhongGiaoSu,
                            text: 'Phòng giáo sư',
                            onpress: () {},
                          ),
                          CustomTextCheckbox(
                            checkIcon: widget.listPhongKham.checkDangKiHen,
                            text: 'Đăng ký hẹn khám',
                            onpress: () {},
                          ),
                        ],
                      ),
                    ),
                  ),
                ]),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 16, horizontal: 32),
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Thông tin hiển thị trên màn hình đăng ký khám tự động",
                      style: TextStyle(
                          color: Color(0xFF4F82C2),
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Card(
                  elevation: 4,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8),
                      side: BorderSide(color: Colors.white)),
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 16,
                      vertical: 8,
                    ),
                    child: Column(
                      children: [
                        CustomTextValue(
                            key1: 'STT', value1: widget.listPhongKham.stt),
                        CustomTextValue(
                            key1: 'STT phòng',
                            value1: widget.listPhongKham.sttPhong),
                        CustomTextValue(
                            key1: 'Chuyên khoa',
                            value1: widget.listPhongKham.chuyenKhoaDrop),
                        CustomTextValue(
                            key1: 'Công khám',
                            value1: widget.listPhongKham.congKham),
                        CustomTextCheckbox(
                          checkIcon: widget.listPhongKham.checkSuDung,
                          text: 'Sử dụng',
                          onpress: () {},
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: Container(
        width: MediaQuery.of(context).size.width,
        height: 64,
        child: Row(
          // mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              width: 16,
            ),
            Expanded(
              flex: 2,
              child: CustomButton(
                  height: 32,
                  textButton: 'Sửa',
                  buttonIcon: SvgPicture.asset('assets/edit.svg'),
                  onPressed: () {
                    Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => AddEditRoom(
                                    checkAddEdit: false,
                                    listPhongKham: widget.listPhongKham)))
                        .then((value) {
                      if (value[1] == 'edit') {
                        loadData(value[0]);
                        setState(() {
                          checkThayDoiEdit = true;
                        });
                      }
                    });
                  }),
            ),
            Container(
              width: 8,
            ),
            Expanded(
              flex: 2,
              child: CustomButton(
                  height: 32,
                  textButton: 'Xóa',
                  buttonIcon: SvgPicture.asset('assets/deny.svg'),
                  onPressed: () {
                    deleteData();
                    Navigator.pop(context, [widget.listPhongKham.id, 'xoa']);
                  }),
            ),
            Container(
              width: 16,
            ),
          ],
        ),
      ),
    );
  }

  Future<void> deleteData() async {
    String apiUrl = 'https://64d2f44d67b2662bf3db88c8.mockapi.io/';
    String param = 'listPhongKham/${widget.listPhongKham.id}';
    await BaseApiNoToken().delete(apiUrl, param);
  }

  void loadData(ListPhongKham value) {
    setState(() {
      widget.listPhongKham = value;
    });
  }
}
