import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:product_layout_app/setting/setting.dart';

import '../componentWidget/appBar/appbar.dart';
import '../homescreen/bottomnavigation.dart';

class accountPage extends StatelessWidget {
  const accountPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: BaseAppBar(
          title: Text('Tài khoản'),
          appBar: AppBar(),
          widgets: [],
          onPress: () => Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => BottomNavigationBarHome())),
        ),
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: 8),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ItemDanhMuc(
                svgPicture: SvgPicture.asset('assets/dangxuat.svg'),
                textItem: 'Đăng xuất',
                onPress: () {},
                color: Color(0xFF6F9BD4),
              ),
            ],
          ),
        ));
  }
}
