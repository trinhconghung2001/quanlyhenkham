import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../Container/DetailSpinner.dart';
import '../componentWidget/appBar/appbar.dart';
import '../homescreen/bottomnavigation.dart';

class settingPage extends StatelessWidget {
  const settingPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: BaseAppBar(
          title: Text('Cài đặt'),
          appBar: AppBar(),
          widgets: [],
          onPress: () => Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => BottomNavigationBarHome())),
        ),
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: 8),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ItemDanhMuc(
                svgPicture: SvgPicture.asset('assets/danhmuc.svg'),
                textItem: 'Danh mục',
                onPress: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              DetailSpinner(value1: 'danhmuc')));
                },
                color: Color(0xFF5CBBB8),
              ),
            ],
          ),
        ));
  }
}

class ItemDanhMuc extends StatelessWidget {
  final SvgPicture svgPicture;
  final Color color;
  final String textItem;
  final VoidCallback onPress;
  const ItemDanhMuc(
      {super.key,
      required this.svgPicture,
      required this.textItem,
      required this.onPress,
      required this.color});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 16),
        child: Card(
            elevation: 4,
            color: color,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(26),
                side: BorderSide(color: Color(0xFFFFFFFF))),
            child: Container(
              height: 80,
              width: MediaQuery.of(context).size.width,
              child: Row(
                children: [
                  Expanded(
                    child: Container(),
                    flex: 1,
                  ),
                  Expanded(
                    child: svgPicture,
                    flex: 1,
                  ),
                  Expanded(
                    child: Container(),
                    flex: 1,
                  ),
                  Expanded(
                    child: Text(
                      textItem,
                      style: TextStyle(color: Color(0xFFFFFFFF)),
                    ),
                    flex: 4,
                  ),
                  Expanded(
                    child: Icon(
                      Icons.arrow_forward_ios_rounded,
                      size: 14,
                      color: Color(0xFFFFFFFF),
                    ),
                  )
                ],
              ),
            )),
      ),
    );
  }
}
