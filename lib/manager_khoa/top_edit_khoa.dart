
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../componentWidget/inputfield/RowTextFieldWidget.dart';
import '../componentWidget/textfieldDropbox/Custom_textfield-dropbox.dart';
import 'drop_box_khoa.dart';

class topEditKhoa extends StatefulWidget {
  final String? ma_khoa;
  final String? ma_khoabyt;
  final String? ten_khoa;
  final String? loai_khoa;
  final String? chuyen_khoa;
  final String? ma_luu_tr;
  const topEditKhoa({super.key, required this.ma_khoa, required this.ma_khoabyt, required this.ten_khoa, required this.loai_khoa, required this.chuyen_khoa, required this.ma_luu_tr});

  @override
  State<topEditKhoa> createState() => _topEditKhoaState();
}

class _topEditKhoaState extends State<topEditKhoa> {
  // late String newValue1;
  // late String newValue2;
  var makhoaController = TextEditingController();
  var makhoaybtController = TextEditingController();
  var tenkhoaController = TextEditingController();
  var maluutruController = TextEditingController();
  // var tenkhoaController = TextEditingController();
  var dropbox1Controller = TextEditingController();
  var dropbox2Controller = TextEditingController();
 
  
  @override
  void initState() {
    super.initState();
    dropbox1Controller.text = widget.loai_khoa!;
    dropbox2Controller.text = widget.chuyen_khoa!;
    makhoaController.text = widget.ma_khoa!;
    makhoaybtController.text = widget.ma_khoabyt!;
    tenkhoaController.text = widget.ten_khoa!;
    maluutruController.text = widget.ma_luu_tr!;
  }
  @override
  Widget build(BuildContext context) {
    double width_new = MediaQuery.of(context).size.width;
    double height_new = MediaQuery.of(context).size.height;
    return Container(
      height: height_new * 0.5,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(8))
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        child: Column(
          
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            RowTextField(
              title: 'Mã khoa', title2: ' (*)', 
              textController: makhoaController,
              onChanged: (p0) => makhoaController.text = p0,
              //onChanged: ,
            ),
            SizedBox(height: height_new * 0.015),
            RowTextField(
              title: 'Mã khoa(BYT)', 
              title2: '', 
              textController: makhoaybtController,
              onChanged: (p0) => makhoaybtController.text = p0,
            ),
            
            SizedBox(height: height_new * 0.015),
            RowTextField(
              title: 'Tên khoa', 
              title2: ' (*)', 
              textController: tenkhoaController,
              onChanged: (p0) => tenkhoaController.text = p0,
            ),
            SizedBox(height: height_new * 0.015),
            TextFieldCheckBox(
              title: 'Loại khoa', 
              textController: dropbox1Controller,
              title2: ' (*)',
              onTap: () async{
                String? selectedValue = await
                Navigator.push( context, 
                  MaterialPageRoute(builder: (context) => dropboxKhoa(value1: 'loaiKhoa'))
                  );
                if(selectedValue != null){
                  setState(() {
                    dropbox1Controller.text = selectedValue;
                  });
                }
              },
            ),
            SizedBox(height: height_new * 0.015),
            TextFieldCheckBox(
              title: 'Chuyên khoa', 
              textController: dropbox2Controller,
              title2: '',
              onTap: () async{
                String? selectedValue = await
                Navigator.push( context, 
                  MaterialPageRoute(builder: (context) => dropboxKhoa(value1: 'chuyenKhoa'))
                  );
                if(selectedValue != null){
                  setState(() {
                    dropbox2Controller.text = selectedValue;
                  });
                }
              },
            ),            
            SizedBox(height: height_new * 0.015),
            RowTextField(
              title: 'Mã lưu trú', 
              title2: '', 
              textController: maluutruController,
              onChanged: (p0) => maluutruController.text = p0,
            ),
          
          ],
        ),
      ),
    );
  }
}

