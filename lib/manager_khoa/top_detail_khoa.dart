import 'package:flutter/material.dart';

import '../componentWidget/inputfield/custom_red_text.dart';

class topDetailKhoa extends StatelessWidget {
  final String? ma_khoa;
  final String? ma_khoabyt;
  final String? ten_khoa;
  final String? loai_khoa;
  final String? chuyen_khoa;
  final String? ma_luu_tr;
  const topDetailKhoa({
    Key? key, 
    required this.ma_khoa,
    required this.ma_khoabyt,
    required this.ten_khoa,
    required this.loai_khoa,
    required this.chuyen_khoa,
    required this.ma_luu_tr
    }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width_new = MediaQuery.of(context).size.width;
    double height_new = MediaQuery.of(context).size.height;

    return Container(
      height: height_new * 0.3,
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(8))
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
        child: Row(
          children: [
            const Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                redText(askText: 'Mã khoa'),
                Text('Mã khoa(BYT)'),
                redText(askText: 'Tên khoa'),
                redText(askText: 'Loại khoa'),
                Text('Chuyên khoa'),
                Text('Mã lưu trú')
              ],
            ),
            SizedBox(width: width_new*0.18),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(ma_khoa!),
                  Text(ma_khoabyt!),
                  Text(ten_khoa!),
                  Text(loai_khoa!),
                  Text(chuyen_khoa!),
                  Text(ma_luu_tr!),
              ],
              )
            )                  
          ],
        )
      ),
    );
  }
}

