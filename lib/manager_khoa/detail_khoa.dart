// ignore_for_file: unused_local_variable

import 'package:flutter/material.dart';
import 'package:product_layout_app/componentWidget/styles/CustomColor.dart';

import '../componentWidget/appBar/appbar.dart';
import '../model/departmentModel.dart';
import 'bot_detail_khoa.dart';
import 'dialog_delete.dart';
import 'test_edit_khoa.dart';
import 'top_detail_khoa.dart';

class detailKhoa extends StatefulWidget {
  final Department dataList;
  const detailKhoa({Key? key, required this.dataList }) : super(key: key);

  @override
  State<detailKhoa> createState() => _detailKhoaState();
}

class _detailKhoaState extends State<detailKhoa> {
  @override
  Widget build(BuildContext context) {
    double width_new = MediaQuery.of(context).size.width;
    double height_new = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: BaseAppBar(
        title: Text('Chi tiết thông tin Khoa'), 
        appBar: AppBar(), 
        widgets: [],
        onPress: () {
          Navigator.of(context).pop();
        },
      ),
      backgroundColor: Color(0xFFF5F5F5),
      body: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              padding: EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  topDetailKhoa(
                    ma_khoa: widget.dataList.idDep, 
                    ma_khoabyt: widget.dataList.idDepYbt, 
                    ten_khoa: widget.dataList.name, 
                    loai_khoa: widget.dataList.type, 
                    chuyen_khoa: widget.dataList.specialty, 
                    ma_luu_tr: widget.dataList.idAddress),
                  SizedBox(height: height_new * 0.01),
                  botDetailKhoa(
                    s1: widget.dataList.ck1,
                    s2: widget.dataList.ck2,
                    s3: widget.dataList.ck3,
                    s4: widget.dataList.ck4,
                    truongkhoa: widget.dataList.managerDep,
                    hangbh: widget.dataList.levelBh,
                    csyt: widget.dataList.csyt,
                    ghichu: widget.dataList.note, 
                  ),
                ],
              ),
            ),
          ),
          Container(
            width: double.infinity,
            height: height_new * 0.07,
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  //flex: 1,
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 8, horizontal: 30),
                    child: ElevatedButton.icon(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Color(0xFF6f9bd4),
                      ),
                      onPressed: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>editKhoa(editList: widget.dataList,)));
                        // Navigator.of(context).push(MaterialPageRoute(builder: (context) => editKhoa(
                        //   // editList: widget.dataList,
                        // )));
                      }, 
                      icon: Icon(
                        Icons.edit_square,
                      ), 
                      label: Text('Sửa')
                    ),
                  )
                ),
                Expanded(
                  //flex: 1,
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 8, horizontal: 30),
                    child: ElevatedButton.icon(
                      style: ElevatedButton.styleFrom(
                        side: const BorderSide(color: Colors.transparent),
                        backgroundColor: primaryColor,
                      ),
                      onPressed: 
                        widget.dataList.state == 'Hủy bỏ' ? null : ()=>
                        showDialog(
                          context: context, 
                          builder: (context){
                            return dialogDelete(idDelete: widget.dataList.id);
                          }
                        )
                      , 
                      icon: Icon(
                        Icons.cancel_outlined,
                        color: Colors.white,
                      ), 
                      label: Text(
                        'Xóa',
                        style: TextStyle(color: Colors.white),
                      )
                    ),
                  )
                )
              ],
            ),
          ),   
        ],
      ),
    );
  }
}
//showAlertDialog 