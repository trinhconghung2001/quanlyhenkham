// import 'package:flutter/material.dart';
// import 'package:te3v72/manager_khoa/appbar.dart';
// import 'package:te3v72/manager_khoa/bottom_edit_khoa.dart';
// import 'package:te3v72/manager_khoa/customAPI.dart';
// import 'package:te3v72/manager_khoa/top_edit_khoa.dart';
// import 'package:te3v72/model/departmentModel.dart';
// import 'package:te3v72/model/khoaModel.dart';
// import 'package:te3v72/qlhk_icons.dart';

// import 'dialog_delete.dart';
// import 'list_khoa.dart';

// class editKhoa extends StatefulWidget {
//   final Department editList;
//   const editKhoa({
//     Key? key, 
//     required this.editList
//   }) : super(key: key);

//   @override
//   State<editKhoa> createState() => _editKhoaState();
// }

// class _editKhoaState extends State<editKhoa> {
//   final formKey = GlobalKey<FormState>();
//   late Future<Department> department;
//   @override
//   Widget build(BuildContext context) {
//     double width_new = MediaQuery.of(context).size.width;
//     double height_new = MediaQuery.of(context).size.height;

//     return GestureDetector(
//       onTap: (){
//         FocusManager.instance.primaryFocus?.unfocus();
//       },
//       child: Scaffold(
//         appBar: BaseAppBar(title: Text('Sửa thông tin Khoa'), appBar: AppBar(), widgets: []),
//         backgroundColor: Color(0xFFF5F5F5),
//         body: Form(
//           key: formKey,
//           child: Column(
//           children: [
//             Expanded(
//               child: SingleChildScrollView(
//                 padding: EdgeInsets.all(16),
//                 child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                   topEditKhoa(
//                     ma_khoa: widget.editList.idDep, 
//                     ma_khoabyt: widget.editList.idDepYbt, 
//                     ten_khoa: widget.editList.name, 
//                     loai_khoa: widget.editList.type, 
//                     chuyen_khoa: widget.editList.specialty, 
//                     ma_luu_tr: widget.editList.idAddress),
//                   SizedBox(height: height_new * 0.01),
//                   bottomEditKhoa(
//                     s1: widget.editList.ck1,
//                     s2: widget.editList.ck2,
//                     s3: widget.editList.ck3,
//                     s4: widget.editList.ck4,
//                     truongkhoa: widget.editList.managerDep,
//                     hangbh: widget.editList.levelBh,
//                     csyt: widget.editList.csyt,
//                     ghichu: widget.editList.note, 
//                   ),
//                 ],
//                 ),
//               ),
//             ),
//             Container(
//               width: double.infinity,
//               height: height_new * 0.07,
//               decoration: BoxDecoration(
//                 color: Colors.white,
//               ),
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                 children: [
//                   Expanded(
//                     //flex: 1,
//                     child: Container(
//                       padding: EdgeInsets.symmetric(vertical: 8, horizontal: 30),
//                       child: ElevatedButton.icon(
//                         style: ElevatedButton.styleFrom(
//                           backgroundColor: Color(0xFF6f9bd4),
//                         ),
//                         onPressed: (){
//                           // Navigator.of(context).pop();
//                           if(formKey.currentState!.validate() )
//                           {
//                             //Navigator.of(context).push(MaterialPageRoute(builder: (context) => listKhoa()));
//                             //formKey.currentState!.save();
//                             //department = updateData('${widget.editList.id}','Vu Quynh', 'Quynh Quynh', '');
//                             Navigator.push(context,MaterialPageRoute(builder: (context) =>listKhoa())); 
//                             print('check');
//                           }else{
//                             print('Lỗi'); 
//                           }
//                         }, 
//                         icon: Icon(Qlhk.save_outlined),
//                         label: Text('Lưu')
//                       ),
//                     )
//                   ),
                  
//                 ],
//               ),
//             ),   
//           ],
//           )
//         ),
//       )
//     );
//   }
// }