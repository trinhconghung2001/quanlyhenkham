import 'package:flutter/material.dart';
import '../componentWidget/dialog/ErrorDialog.dart';
import '../model/departmentModel.dart';
import '../componentWidget/API/customAPI.dart';
import 'test_list_khoa.dart';

class dialogDelete extends StatefulWidget {
  final String? idDelete;
  const dialogDelete({
    Key? key, 
    required this.idDelete
    }) : super(key: key);

  @override
  State<dialogDelete> createState() => _dialogDeleteState();
}


class _dialogDeleteState extends State<dialogDelete> {
  late Future<Department> department;
  
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ErrorDialog(
        title: 'XÁC NHẬN',
        message: 'Bạn có chắc chắn muốn xóa Khoa không?',
        buttonText: 'Xác nhận',
        //icon: Qlhk.cancel,
        onPress: () {

          //department = updateData(widget.idDelete!,'', '', 'Hủy bỏ');
          department = deleteData(widget.idDelete!, 'Hủy bỏ');
          //Navigator.pop(context);
          Navigator.push(context,MaterialPageRoute(builder: (context) =>listKhoa()));
        },
      ),
    );
  }
}
