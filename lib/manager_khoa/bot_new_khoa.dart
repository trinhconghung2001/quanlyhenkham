// import 'package:flutter/material.dart';

// import '../componentWidget/checkbox/custom_edit_checkbox.dart';
// import '../componentWidget/inputfield/TextAreaWidget.dart';
// import '../componentWidget/textfieldDropbox/Custom_textfield-dropbox.dart';
// import 'drop_box_khoa.dart';

// class bottomNewKhoa extends StatefulWidget {
//   const bottomNewKhoa({super.key});

//   @override
//   State<bottomNewKhoa> createState() => _bottomNewKhoaState();
// }

// class _bottomNewKhoaState extends State<bottomNewKhoa> {
//   var dropbox1Controller = TextEditingController(text: '--Lựa chọn--');
//   var dropbox2Controller = TextEditingController(text: '--Lựa chọn--');
//   var dropbox3Controller = TextEditingController(text: '--Lựa chọn--');

//   @override
//   Widget build(BuildContext context) {
//     double width_new = MediaQuery.of(context).size.width;
//     double height_new = MediaQuery.of(context).size.height;

//     return Container(
//       height: height_new * 0.65,
//       decoration: BoxDecoration(
//         color: Colors.white,
//         borderRadius: BorderRadius.all(Radius.circular(8))
//       ),
//       child: Padding(
//         padding: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
//         child: Column(
//           children: [
//             Container(
//               margin: EdgeInsets.symmetric(vertical: 8),
//               child: Column(
//                 children:
//                 [
//                   editCheckBox(checks: false, title: 'Nhận cấp cứu'),
//                   editCheckBox(checks: false, title: 'Nhận PTTT'),
//                   editCheckBox(checks: false, title: 'Chọn giường khi chuyển đến'),
//                   editCheckBox(checks: false, title: 'Tính hóa đơn theo nguồn hoạch toán')                
//                 ], 
//               ),
//             ),
//             Container(
//               padding: EdgeInsets.symmetric(vertical: 8, horizontal: 10),
//             child: Column( 
//               mainAxisAlignment: MainAxisAlignment.spaceBetween,           
//               children: [ 
//                 TextFieldCheckBox(
//                   title: 'Trưởng khoa', 
//                   textController: dropbox1Controller,
//                   title2: '',
//                   onTap: () async{
//                     String? selectedValue = await
//                     Navigator.push( context, 
//                       MaterialPageRoute(builder: (context) => dropboxKhoa(value1: 'truongKhoa'))
//                       );
//                     if(selectedValue != '--Lựa chọn--' && selectedValue != null){
//                       setState(() {
//                         dropbox1Controller.text = selectedValue;
//                       });
//                     }
//                   },
//                 ),
//                 SizedBox(height: height_new * 0.015),
//                 TextFieldCheckBox(
//                   title: 'Hạng BH trái tuyến', 
//                   textController: dropbox2Controller,
//                   title2: '',
//                   onTap: () async{
//                     String? selectedValue = await
//                     Navigator.push( context, 
//                       MaterialPageRoute(builder: (context) => dropboxKhoa(value1: 'hangBH'))
//                       );
//                     if(selectedValue != '--Lựa chọn--' && selectedValue != null){
//                       setState(() {
//                         dropbox2Controller.text = selectedValue;
//                       });
//                     }
//                   },
//                 ),
//                 SizedBox(height: height_new * 0.015),
//                 TextFieldCheckBox(
//                   title: 'Cơ sở y tế', 
//                   textController: dropbox3Controller,
//                   title2: '',
//                   onTap: () async{
//                     String? selectedValue = await
//                     Navigator.push( context, 
//                       MaterialPageRoute(builder: (context) => dropboxKhoa(value1: 'csyt'))
//                       );
//                     if(selectedValue != '--Lựa chọn--' && selectedValue != null){
//                       setState(() {
//                         dropbox3Controller.text = selectedValue;
//                       });
//                     }
//                   },
//                 ),
//                 SizedBox(height: height_new * 0.015),
//                 TextArea(title: 'Ghi chú'),
//               ],
//             )
//           ),
//           ],
//         ),
//       )     
//     );
//   }
// }