import 'package:flutter/material.dart';

import '../styles/CustomColor.dart';

class listCheckBox extends StatelessWidget {
  final bool? value;
  final String title;
  const listCheckBox({super.key, required this.value, required this.title});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      //leading: value == 'true' ? Icon(Icons.check_box, color: primaryColor) : Icon(Icons.check_box_outline_blank),
      leading: value == true ? Icon(Icons.check_box, color: primaryColor) : Icon(Icons.check_box_outline_blank),
      title: Text(
        title,
        style: TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.w400
        ),
      ),
      visualDensity: VisualDensity(vertical: -3),
);
  }
}