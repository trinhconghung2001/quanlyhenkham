// import 'package:flutter/material.dart';

// class rowTextField extends StatefulWidget {
//   final String title1;
//   final String title2;
//   //final bool text;

//   const rowTextField({
//     Key? key, 
//     required this.title1, 
//     required this.title2,
//    //this.text=false
//    }): super(key: key);

//   @override
//   State<rowTextField> createState() => _rowTextFieldState();
// }

// class _rowTextFieldState extends State<rowTextField> {
//   bool _text = false;

//   @override
//   // void initState() {
//   //   text = false;
//   // }
//   Widget build(BuildContext context) {
//     double width_new = MediaQuery.of(context).size.width;
//     double height_new = MediaQuery.of(context).size.height;
//     return Row(
//       children: [
//         Expanded(
//           flex: 1,
//           child: Column(
//             children: [
//               Align(
//                 alignment: Alignment.topLeft,
//                 child: Text(
//                   widget.title1,
//                   style: TextStyle(color: Color(0xFF535858)),
//                   ),
//               ),
//               SizedBox(height: height_new*0.01),
//               Container(
//                 height: height_new*0.05,
//                 child: TextFormField(
//                   decoration: InputDecoration(
//                     enabledBorder: OutlineInputBorder(
//                       borderSide: BorderSide(
//                         color: Colors.transparent,
//                       ),
//                       borderRadius: BorderRadius.circular(8),
//                     ),
//                     filled: true,
//                     fillColor: Colors.white,
//                     //suffixIcon: Icon(Icons.cancel_outlined, size:20)
//                     suffixIcon: IconButton(
//                       onPressed: (){
//                         setState(() {
//                           _text = !_text;
//                         });
//                       }, 
//                       icon: Icon(
//                         _text ? Icons.cancel_outlined:Icons.abc_outlined,
//                         size: 20,
//                       )
//                     )
//                   ),
//                 ),
//               )
//             ],
//           )
//         ),
//         SizedBox(width: width_new*0.02),
//         Expanded(
//           flex: 1,
//           child: Column(
//             children: [
//               Align(
//                 alignment: Alignment.topLeft,
//                 child: Text(
//                   widget.title2,
//                   style: TextStyle(color: Color(0xFF535858)),
//                   ),
//               ),
//               SizedBox(height: height_new*0.01),
//               Container(
//                 height: height_new*0.05,
//                 child: TextFormField(
//                   decoration: InputDecoration(
//                     enabledBorder: OutlineInputBorder(
//                       borderSide: BorderSide(
//                         color: Colors.transparent,
//                       ),
//                       borderRadius: BorderRadius.circular(8),
//                     ),
//                     filled: true,
//                     fillColor: Colors.white,
//                     suffixIcon: Icon(Icons.cancel_outlined, size:20)
//                   ),
//                 ),
//               )   
//             ],
//           )
//         )
//       ],
//     );
//   }
// }