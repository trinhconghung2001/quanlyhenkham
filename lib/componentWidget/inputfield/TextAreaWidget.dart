import 'package:flutter/material.dart';

import '../styles/CustomColor.dart';
import '../styles/Dimens.dart';

class TextArea extends StatefulWidget {
  final String title;
  final Function(String)? onChanged;
  final TextEditingController? textController;
  const TextArea({
    Key? key,
    required this.title,
    this.textController,
    this.onChanged,
  }) : super(key: key);

  @override
  State<TextArea> createState() => _TextAreaState();
}

class _TextAreaState extends State<TextArea> {
  late String check;
  @override
  Widget build(BuildContext context) {
    
    return Row(
      children: <Widget>[
        Expanded(
          flex: 4,
          child: Align(
            alignment: Alignment.topLeft,
            child: Text(
              widget.title,
              style: new TextStyle(
                color: Color(0xFF535858),
                fontSize: 14,
                fontWeight: FontWeight.w400,
              ),
              textAlign: TextAlign.left,
            ),
          ),
        ),
        Expanded(
          flex: 6,
          child: Container(
            padding: EdgeInsets.only(top: 4.0),
            child: TextField(
              minLines: 2,
              maxLines: 2,
              style: TextStyle(
                color: Color(0xFF444444),
                fontSize: 14,
                fontWeight: FontWeight.w400,
              ),
              controller: widget.textController,
              cursorColor: primaryColor,
              onChanged: (text) => setState(() => check = text),
              decoration: new InputDecoration(
                
                contentPadding: const EdgeInsets.only(
                    left: defaultTextInsideBoxPadding,
                    right: defaultTextInsideBoxPadding),
                fillColor: Colors.white,
                enabledBorder: const OutlineInputBorder(
                  borderSide:
                      const BorderSide(color: Colors.grey, width: 1.0),
                ),
                border: new OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(4.0),
                ),
                focusedBorder: const OutlineInputBorder(
                  borderSide: BorderSide(
                    color: primaryColor, width: 2.0
                  )
                )
              ),
            ),
          ),
        ),
      ],
    );
  }
}
