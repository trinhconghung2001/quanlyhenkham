import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class redText extends StatelessWidget {
  final String askText;
  const redText({super.key, required this.askText});

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        text: askText,
        style: TextStyle(
          color: Colors.black,        
        ),
        children: [
          TextSpan(
            text: ' (*)',
            style: TextStyle(
              color: Colors.red,
            )
          )
        ]
      )
    );
  }
}