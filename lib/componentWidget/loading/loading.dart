import 'package:flutter/material.dart';
import '../styles/CustomColor.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Loader extends StatelessWidget {
  const Loader({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SpinKitFadingCircle(
        itemBuilder: (BuildContext context, int index){
          return DecoratedBox(
            decoration: BoxDecoration(
              color: index.isEven ? primaryColor : Colors.white,
            )
          );
        },
        size: 40,
      ),
    );
  }
}