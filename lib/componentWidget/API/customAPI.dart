
import 'package:http/http.dart' as http;

import 'dart:convert';

import '../../model/departmentModel.dart';

String apiUrl = 'https://64d2f05867b2662bf3db84c5.mockapi.io/api/flutter';

Future<Department> fetchData() async {
  final response = await http.get(Uri.parse('${(apiUrl)}/department'));
  if (response.statusCode == 200) {
    return Department.fromJson(jsonDecode(response.body));
  } else {
    throw Exception('Failed to load DepartmentAPI');
  }
}

Future<Department> addData(Department body) async {
  final response = await http.post(
    Uri.parse('${(apiUrl)}/department')
   ,  
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    }, 
    body: json.encode(body.toJson()),
  );

  if (response.statusCode == 201) {
    print(response.body);
    return Department.fromJson(jsonDecode(response.body));
    
  } else {
     throw Exception('Failed to load DepartmentAPI');
  }
}

Future<Department> updateData(String id,Department body) async {
  final response = await http.put(
    Uri.parse('${(apiUrl)}/department/${(id)}')
   ,  
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    }, 
    body: json.encode(body.toJson()),
  );

  if (response.statusCode == 200) {
    print(response.body);
    return Department.fromJson(jsonDecode(response.body));
    
  } else {
     throw Exception('Failed to load DepartmentAPI');
  }
}

Future<Department> deleteData(String id, String state) async {
  final response = await http.put(
    Uri.parse('${(apiUrl)}/department/${(id)}')
   ,  
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    }, 
    body: jsonEncode(<String, dynamic>{
      'state':state
    }),
  );

  if (response.statusCode == 200) {
    print(response.body);
    return Department.fromJson(jsonDecode(response.body));
    
  } else {
     throw Exception('Failed to load DepartmentAPI');
  }
}

