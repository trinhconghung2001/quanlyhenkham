import 'dart:convert';

import 'package:flutter/material.dart';
// import 'package:test_item/timeSchedule/addHour.dart';

import '../api/callapi.dart';
import '../item/ItemListRangeHour.dart';
import '../model/time.dart';
import 'listSearch.dart';

class CreateTimes extends StatefulWidget {
  const CreateTimes({super.key});

  @override
  State<CreateTimes> createState() => _CreateTimesState();
}

class _CreateTimesState extends State<CreateTimes> {
  List<Times> listItem = [];
  List<Times> responseAPI = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    callAPI();
  }

  void initDataAgain() {
    setState(() {
      listItem = responseAPI;
    });
  }

  void searchSchedule(value) {
    print('vao tim kiem ow day');
    List<Times> tmp = listItem;
    List<Times> listSearch = [];
    for (int i = 0; i < listItem.length; i++) {
      if ((listItem[i].timeStartAM!.toLowerCase().contains(value[0]) ||
              listItem[i].timeEndAM!.contains(value[0])) &&
          (listItem[i].timeStartPM!.contains(value[1]) ||
              listItem[i].timeEndPM!.contains(value[1])) &&
          listItem[i].time!.contains(value[2]) &&
          listItem[i].amountPatient!.contains(value[3])) {
        listSearch.add(listItem[i]);
      }
    }
    setState(() {
      listItem = listSearch;
    });
  }

  void callAPI() async {
    try {
      String url =
          'https://64d2e2e667b2662bf3db7e0a.mockapi.io/api/test/time_appointment/';
      final result = await BaseAPI().get(url);
      final List<dynamic> jsonList = json.decode(result);
      final List<Times> timeBlocks =
          jsonList.map((json) => Times.fromJson(json)).toList();
      setState(() {
        listItem = timeBlocks;
        responseAPI = timeBlocks;
      });
    } catch (e) {
      print('==================\nLoi goi api' + e.toString());
    }
  }

  void APIupdateTrangThai(id, times) async {
    try {
      String url =
          'https://64d2e2e667b2662bf3db7e0a.mockapi.io/api/test/time_appointment/';
      final result = await BaseAPI().updateStateAppointment(url, id, times);
    } catch (e) {
      print('============================================\nco loi' +
          e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: const Color(0xFFEEEEEE),
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: Color(0xFF6F9BD4),
          title: const Text(
            'Quản lý thời gian khám',
            style: TextStyle(color: Colors.white, fontSize: 16),
          ),
        ),
        body: Center(
          child: listItem.length > 0
              ? ListView.builder(
                  itemCount: listItem.length,
                  itemBuilder: (BuildContext context, int index) {
                    if (index == 0) {
                      return Column(
                        children: [
                          ListSearchItem(
                            onCLickAdd: () {
                              print('vao goi api lai');
                              callAPI();
                            },
                            onCLickSearch: (value) {
                              initDataAgain();
                              searchSchedule(value);
                            },
                          ),
                          HourManage(
                            callbackData: (value) {
                              if (value == 'refresh') {
                                callAPI();
                              }
                            },
                            time: listItem[index],
                            onCLickIcon: (value) {
                              setState(() {
                                listItem[index].isApply = value;
                                print(listItem[index].isApply.toString());
                              });
                              APIupdateTrangThai(
                                  listItem[index].id, listItem[index]);
                            },
                          )
                        ],
                      );
                    }
                    return HourManage(
                      callbackData: (value) {
                        if (value == 'refresh') {
                          callAPI();
                        }
                      },
                      time: listItem[index],
                      onCLickIcon: (value) {
                        setState(() {
                          listItem[index].isApply = value;
                          APIupdateTrangThai(
                              listItem[index].id, listItem[index]);
                        });
                      },
                    );
                  })
              : Column(
                  children: [
                    Expanded(
                      flex: 4,
                      child: ListSearchItem(
                        onCLickAdd: () {
                          print('vao goi api lai');
                          callAPI();
                        },
                        onCLickSearch: (value) {
                          initDataAgain();
                          searchSchedule(value);
                        },
                      ),
                    ),
                    Expanded(
                        flex: 8, child: Center(child: Text('Khong co data')))
                  ],
                ),
        ),
      ),
    );
  }
}
