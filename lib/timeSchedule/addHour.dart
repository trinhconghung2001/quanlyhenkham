import 'package:flutter/material.dart';
// import 'package:test_item/api/callapi.dart';
// import 'package:test_item/model/timedetail.dart';
// import 'package:test_item/timeSchedule/edit_textformfield.dart';

import '../api/callapi.dart';
import '../item/box_decoration_container.dart';
import '../item/elevated_button.dart';
import '../item/line_dropbutton.dart';
import '../model/time.dart';
import 'edit_textformfield.dart';

class AddSchedule extends StatefulWidget {
  const AddSchedule({super.key});
  @override
  State<AddSchedule> createState() => _AddScheduleState();
}

class _AddScheduleState extends State<AddSchedule> {
  final _textEditingController = TextEditingController();
  String timeBeginAM = '';
  String timeFinishAM = '';
  String timeBeginPM = '';
  String timeFinishPM = '';
  String time = '';
  String amountPatient = '';
  String type = '';
  List<Hourlist> listHour = [];
  void addTime() async {
    // String hou1 = timeBeginAM[0] + timeBeginAM[1];
    // String hour2 = timeBeginAM[0] + timeFinishAM[1];
    // String hour3 = timeBeginPM[0] + timeBeginPM[1];
    // String hour4 = timeBeginPM[0] + timeFinishPM[1];
    if (int.parse(timeBeginAM[0] + timeBeginAM[1]) >
            int.parse(timeFinishAM[0] + timeFinishAM[1]) ||
        int.parse(timeBeginPM[0] + timeBeginPM[1]) >
            int.parse(timeFinishPM[0] + timeFinishPM[1])) {
      print('thoi gian khong hop le');
    } else {
      try {
        String url =
            'https://64d2e2e667b2662bf3db7e0a.mockapi.io/api/test/time_appointment/';
        Times times = Times(
            timeStartAM: timeBeginAM,
            timeEndAM: timeFinishAM,
            timeStartPM: timeBeginPM,
            timeEndPM: timeFinishPM,
            time: time,
            amountPatient: amountPatient,
            note: _textEditingController.text.toString(),
            state: type == 'Khám tại cơ sở' ? 1 : 0,
            isApply: true,
            hourlist: listHour,
            id: '1');
        final resp = await BaseAPI().addTimeAppointment(url, times);
        Navigator.of(context).pop('refresh');
      } catch (e) {
        print('============\nloi api add ' + e.toString());
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        backgroundColor: const Color(0xFFEEEEEE),
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: Color(0xFF6F9BD4),
          title: const Text(
            'Thêm mới lịch trình khám',
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
        ),
        body: Scaffold(
          bottomNavigationBar: Container(
            height: 40,
            padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: CustomElevatedButton(
                onClickIcon: () {
                  String note = _textEditingController.text;
                  print(note);
                  if (timeBeginAM == '' ||
                      timeFinishAM == '' ||
                      timeBeginPM == '' ||
                      timeFinishPM == '' ||
                      time == '' ||
                      amountPatient == '' ||
                      type == '') {
                    print('khong duoc de trong');
                  } else {
                    addTime();
                  }
                },
                text: 'Lưu',
                icon: Icons.save_alt,
                color: Color(0xFF6F9BD4),
                colorText: Colors.white),
          ),
          body: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.all(8),
                  decoration: BoxecorationContainer,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      LineInput(
                          title: 'TG BĐ sáng (*)',
                          text: ' ' + timeBeginAM,
                          keyData: 'BeginTimeAM',
                          getData: (value) {
                            setState(() {
                              timeBeginAM = value;
                            });
                          }),
                      LineInput(
                          title: 'TG KT sáng (*)',
                          text: timeFinishAM,
                          keyData: 'FinishTimeAM',
                          getData: (value) {
                            setState(() {
                              timeFinishAM = value;
                            });
                          }),
                      LineInput(
                          title: 'TG BĐ chiều (*)',
                          text: timeBeginPM,
                          keyData: 'BeginTimePM',
                          getData: (value) {
                            setState(() {
                              timeBeginPM = value;
                            });
                          }),
                      LineInput(
                          title: 'TG KT chiều (*)',
                          text: timeFinishPM,
                          keyData: 'FinishTimePM',
                          getData: (value) {
                            setState(() {
                              timeFinishPM = value;
                            });
                          }),
                      LineInput(
                          title: 'TG khám TB/ BN ',
                          text: time,
                          keyData: 'TimePerPatient',
                          getData: (value) {
                            setState(() {
                              time = value;
                            });
                          }),
                      LineInput(
                          title: 'SL BN/ thời điểm ',
                          text: amountPatient,
                          keyData: 'AmountPatient',
                          getData: (value) {
                            setState(() {
                              amountPatient = value;
                            });
                          }),
                      LineInput(
                          title: 'Loại khám (*)',
                          text: type,
                          keyData: 'Type',
                          getData: (value) {
                            setState(() {
                              type = value;
                            });
                          }),
                      Padding(
                        padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
                        child: EditTextFormField(
                            textEditingController: _textEditingController,
                            title: 'Ghi chú',
                            flexLeft: 1,
                            flexRight: 1),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
