import 'package:flutter/material.dart';
// import 'package:test_item/item/box_search.dart';
// import 'package:test_item/item/elevated_button.dart';

import '../item/box_search.dart';
import '../item/custom_button.dart';
import '../item/itemInputSearch.dart';
import 'addHour.dart';

class ListSearchItem extends StatefulWidget {
  const ListSearchItem(
      {required this.onCLickAdd, required this.onCLickSearch, super.key});
  final Function() onCLickAdd;
  final Function(List<String>) onCLickSearch;
  @override
  State<ListSearchItem> createState() => _ListSearchItemState();
}

class _ListSearchItemState extends State<ListSearchItem> {
  final _focusTgianSang = FocusNode();
  final _focusTgianChieu = FocusNode();
  final _focusTgianBN = FocusNode();
  final _focusBN_TD = FocusNode();

  final _textEdittingSang = TextEditingController();
  final _textEdittingChieu = TextEditingController();
  final _textEdittingTGBN = TextEditingController();
  final _textEdittingBNTD = TextEditingController();
  @override
  void initState() {
    super.initState();
    _focusTgianSang.addListener(_onFocusChange);
    _focusTgianChieu.addListener(_onFocusChange);
    _focusTgianBN.addListener(_onFocusChange);
    _focusBN_TD.addListener(_onFocusChange);
  }

  List<String> result = ['', '', '', ''];
  String dataFromChild = '';
  void _onFocusChange() {
    setState(() {
      result[0] = _textEdittingSang.text;
      result[1] = _textEdittingChieu.text;
      result[2] = _textEdittingTGBN.text;
      result[3] = _textEdittingBNTD.text;
    });
  }

  @override
  void dispose() {
    super.dispose();
    _focusTgianSang.dispose();
    _focusTgianChieu.dispose();
    _focusTgianBN.dispose();
    _focusBN_TD.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(15, 10, 15, 0),
      decoration: BoxDecorationSearch,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Expanded(
                  child: ItemSearchInput(
                title: 'Thời gian sáng',
                focusNode: _focusTgianSang,
                textEditingController: _textEdittingSang,
              )),
              Expanded(
                  child: ItemSearchInput(
                title: 'Thời gian chiều',
                focusNode: _focusTgianChieu,
                textEditingController: _textEdittingChieu,
              ))
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                  child: ItemSearchInput(
                title: 'Thời gian khám/ BN',
                focusNode: _focusTgianBN,
                textEditingController: _textEdittingTGBN,
              )),
              Expanded(
                  child: ItemSearchInput(
                title: 'SL BN/ thời điểm',
                focusNode: _focusBN_TD,
                textEditingController: _textEdittingBNTD,
              )),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              CustomButton(
                  text: 'Thêm mới',
                  icon: Icons.add_circle_outline_outlined,
                  onClick: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => AddSchedule())).then((value) {
                      if (value == 'refresh') {
                        print('vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv');
                        widget.onCLickAdd();
                      }
                    });
                  }),
              CustomButton(
                  text: 'Tìm kiếm',
                  icon: Icons.search_sharp,
                  onClick: () {
                    widget.onCLickSearch(result);
                    print('vao onclick');
                    print('ketqua:' +
                        result[0] +
                        ' ' +
                        result[1] +
                        ' ' +
                        result[2] +
                        ' ' +
                        result[3]);
                  })
            ],
          ),
          const SizedBox(
            height: 10,
          )
        ],
      ),
    );
  }
}
