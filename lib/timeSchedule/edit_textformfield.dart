import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class EditTextFormField extends StatelessWidget {
  EditTextFormField(
      {required this.textEditingController,
      required this.title,
      this.flexLeft,
      this.flexRight,
      super.key});
  TextEditingController textEditingController;
  final String title;
  final int? flexLeft;
  final int? flexRight;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Expanded(
              flex: flexLeft ?? 4,
              child: Container(
                  child: Text(title,
                      style: TextStyle(
                          fontWeight: FontWeight.w400, fontSize: 12)))),
          Expanded(
              flex: flexRight ?? 6,
              child: Container(
                child: TextFormField(
                  controller: textEditingController,
                  style: TextStyle(fontSize: 12),
                  maxLines: 2,
                  textAlign: TextAlign.start,
                  decoration: const InputDecoration(
                      focusedBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(width: 0.5, color: Colors.black)),
                      fillColor: Colors.blue,
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          borderSide:
                              BorderSide(color: Colors.grey, width: 0.5))),
                ),
              ))
        ],
      ),
    );
  }
}
