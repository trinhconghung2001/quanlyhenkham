import 'package:flutter/material.dart';
// import 'package:test_item/item/RowTextView.dart';

import '../dialog/delete_dialog.dart';
import '../item/RowTextView.dart';
import '../item/box_decoration_container.dart';
import '../item/elevated_button.dart';
import '../item/line_text.dart';
import '../model/time.dart';
import 'edit_schedule.dart';
import 'hourList.dart';

class ScheduleDetail extends StatefulWidget {
  const ScheduleDetail({required this.timeschedule, super.key});
  final Times timeschedule;

  @override
  State<ScheduleDetail> createState() => _ScheduleDetailState();
}

class _ScheduleDetailState extends State<ScheduleDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFEEEEEE),
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: Color(0xFF6F9BD4),
        title: const Text(
          'Chi tiết lịch khám',
          style: TextStyle(color: Colors.white, fontSize: 16),
        ),
      ),
      body: Scaffold(
        bottomNavigationBar: Container(
            height: 40,
            padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                CustomElevatedButton(
                    onClickIcon: () {
                      showDialog(
                          context: context,
                          builder: (context) {
                            return AlertPopup(
                              id: widget.timeschedule.id!,
                              url:
                                  'https://64d2e2e667b2662bf3db7e0a.mockapi.io/api/test/time_appointment/',
                            );
                          }).then((value) {
                        Navigator.of(context).pop(value);
                      });
                    },
                    text: 'Xóa',
                    icon: Icons.save_alt,
                    color: Colors.white,
                    colorText: Color(0xFF6F9BD4)),
                CustomElevatedButton(
                    onClickIcon: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => EditTimeAppointment(
                                    editTimes: widget.timeschedule,
                                  ))).then((value) {
                        Navigator.of(context).pop(value);
                      });
                    },
                    text: 'Sửa',
                    icon: Icons.edit_note_outlined,
                    color: Color(0xFF6F9BD4),
                    colorText: Colors.white),
                CustomElevatedButton(
                    onClickIcon: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ListHour(
                                    time: widget.timeschedule,
                                  ))).then((value) {
                        Navigator.of(context).pop(value);
                      });
                    },
                    text: 'Chi tiết thời gian',
                    icon: Icons.save_alt,
                    color: Color(0xFF6F9BD4),
                    colorText: Colors.white),
              ],
            )),
        body: Container(
            margin: const EdgeInsets.all(10),
            padding: EdgeInsets.all(10),
            decoration: BoxecorationContainer,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  RowTextView(
                    lable: 'Trạng thái',
                    content: widget.timeschedule.isApply!
                        ? 'Đang áp dụng'
                        : 'Chưa áp dụng',
                    flexItemLeft: 6,
                    flexItemRight: 4,
                    color2:
                        widget.timeschedule.isApply! ? Colors.blue : Colors.red,
                  ),
                  RowTextView(
                    lable: 'Thời gian bắt đầu sáng',
                    content: widget.timeschedule.timeStartAM!,
                    flexItemLeft: 6,
                    flexItemRight: 4,
                  ),
                  RowTextView(
                    lable: 'Thời gian kết thúc sáng',
                    content: widget.timeschedule.timeEndAM!,
                    flexItemLeft: 6,
                    flexItemRight: 4,
                  ),
                  RowTextView(
                    lable: 'Thời gian bắt đầu chiều',
                    content: widget.timeschedule.timeStartPM!,
                    flexItemLeft: 6,
                    flexItemRight: 4,
                  ),
                  RowTextView(
                    lable: 'Thời gian kết thúc chiều',
                    content: widget.timeschedule.timeEndPM!,
                    flexItemLeft: 6,
                    flexItemRight: 4,
                  ),
                  RowTextView(
                    lable: 'Thời gian khám /BN',
                    content: widget.timeschedule.time!,
                    flexItemLeft: 6,
                    flexItemRight: 4,
                  ),
                  RowTextView(
                    lable: 'Số lượng BN/ thời điểm',
                    content: widget.timeschedule.amountPatient!,
                    flexItemLeft: 6,
                    flexItemRight: 4,
                  ),
                  RowTextView(
                    lable: 'Loại khám',
                    content: widget.timeschedule.state == 1
                        ? 'Khám tại cơ sở'
                        : 'Tư vấn từ xa',
                    flexItemLeft: 6,
                    flexItemRight: 4,
                  ),
                  RowTextView(
                    lable: 'Ghi chú',
                    content: widget.timeschedule.note!,
                    flexItemLeft: 6,
                    flexItemRight: 4,
                  ),
                ],
              ),
            )),
      ),
    );
  }
}
