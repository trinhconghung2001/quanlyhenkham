import 'dart:convert';

import 'package:flutter/material.dart';
// import 'package:test_item/api/callapi.dart';
// import 'package:test_item/item/elevated_button.dart';
// import 'package:test_item/item/line_start_hour.dart';
// import 'package:test_item/test_vo_van.dart';

import '../api/callapi.dart';
import '../item/ItemListHour.dart';
import '../item/elevated_button.dart';
import '../item/line_start_hour.dart';
import '../item/search_text.dart';
import '../model/time.dart';
import '../model/timedetail.dart';

class ListHour extends StatefulWidget {
  const ListHour({required this.time, super.key});
  final Times time;
  @override
  State<ListHour> createState() => _ListHourState();
}

class _ListHourState extends State<ListHour> {
  List<Hourlist> listItem = [];
  List<Hourlist> dataBackup = [];
  TextEditingController textEditingController = TextEditingController();
  late bool isCheckAll;
  @override
  void initState() {
    super.initState();
    initData();
    initCheckAll();
  }

  initCheckAll() {
    bool CheckAll = false;
    for (int i = 0; i < listItem.length; i++) {
      if (listItem[i].check == false) {
        CheckAll = false;
      }
    }
    setState(() {
      isCheckAll = CheckAll;
    });
  }

  initData() async {
    final result = widget.time.hourlist!;
    for (int i = 0; i < result.length; i++) {
      listItem.add(Hourlist(
          id: result[i].id,
          stt: result[i].stt,
          check: result[i].check,
          time: result[i].time));
    }
    dataBackup = listItem;
  }

  initDataAgain() {
    setState(() {
      listItem = dataBackup;
    });
  }

  resetData(bool value) {
    for (int i = 0; i < listItem.length; i++) {
      setState(() {
        listItem[i].check = value;
      });
    }
  }

  void _changeState(value) {
    setState(() {
      isCheckAll = value;
      resetData(isCheckAll);
    });
  }

  void _changeStateLine(value, index) {
    print(value.runtimeType);
    setState(() {
      listItem[index].check = value;
      bool CheckAll = true;
      for (int i = 0; i < listItem.length; i++) {
        if (listItem[i].check == false) {
          CheckAll = false;
        }
      }
      setState(() {
        isCheckAll = CheckAll;
      });
    });
  }

  searchItem() {
    String input = textEditingController.text;
    print(input);
    List<Hourlist> listSearch = [];
    for (int i = 0; i < listItem.length; i++) {
      if (listItem[i]
              .stt
              .toString()
              .toLowerCase()
              .contains(input.toLowerCase()) ||
          listItem[i].time!.contains(input)) {
        listSearch.add(listItem[i]);
      }
    }
    setState(() {
      listItem = listSearch;
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        // resizeToAvoidBottomInset: false,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: Color(0xFF6F9BD4),
          title: const Text(
            'Chi tiết giờ khám',
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
        ),

        body: Scaffold(
          bottomNavigationBar: Container(
            margin: EdgeInsets.fromLTRB(10, 10, 10, 5),
            height: 32,
            child: CustomElevatedButton(
              onClickIcon: () {
                updateHourList();
              },
              text: 'Cập nhật',
              icon: Icons.add_outlined,
              color: Color(0xFF6F9BD4),
              colorText: Colors.white,
            ),
          ),
          body: Container(
            margin: const EdgeInsets.all(10),
            child: listItem.length > 0
                ? ListView.builder(
                    itemCount: listItem.length,
                    itemBuilder: (BuildContext context, int index) {
                      if (index == 0) {
                        return Column(
                          children: [
                            InputSearch(
                                textEditingController: textEditingController,
                                onClickSearch: () {
                                  print('vao search');
                                  initDataAgain();
                                  searchItem();
                                }),
                            LineStart(
                                isCheckAll: isCheckAll,
                                onClickIcon: (value) {
                                  _changeState(value);
                                }),
                            HourItemList(
                              isCheck: listItem[index].check.toString(),
                              onItemClick: (ValueChanged) {
                                _changeStateLine(ValueChanged, index);
                              },
                              stt: listItem[index].stt.toString(),
                              tgian: listItem[index].time!,
                            )
                          ],
                        );
                      }
                      return HourItemList(
                        isCheck: listItem[index].check.toString(),
                        onItemClick: (ValueChanged) {
                          _changeStateLine(ValueChanged, index);
                        },
                        stt: listItem[index].stt.toString(),
                        tgian: listItem[index].time.toString(),
                      );
                    })
                : Column(
                    children: [
                      InputSearch(
                          textEditingController: textEditingController,
                          onClickSearch: () {
                            print('vao search');
                            initDataAgain();
                            searchItem();
                          }),
                      LineStart(
                          isCheckAll: isCheckAll,
                          onClickIcon: (value) {
                            _changeState(value);
                          }),
                    ],
                  ),
          ),
        ),
      ),
    );
  }

  void updateHourList() async {
    try {
      String url =
          'https://64d2e2e667b2662bf3db7e0a.mockapi.io/api/test/time_appointment/';
      setState(() {
        widget.time.hourlist = listItem;
      });
      final results = await BaseAPI()
          .updateStateAppointment(url, widget.time.id!, widget.time);
      Navigator.of(context).pop('refresh');
    } catch (e) {
      print(
          '=============================\nloi update hour list' + e.toString());
    }
  }
}
