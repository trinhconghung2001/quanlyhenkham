import 'package:flutter/material.dart';
// import 'package:test_item/api/callapi.dart';
// import 'package:test_item/item/CustomDropDown.dart';
// import 'package:test_item/vatTu/item_edit_dvt.dart';

import '../api/callapi.dart';
import '../item/elevated_button.dart';
import '../item/line_dropbutton.dart';
import '../item/line_text_border.dart';
import '../model/dvt.dart';
import 'item_edit_dvt.dart';

class EditDVT extends StatefulWidget {
  const EditDVT({required this.dvt, super.key});
  final DuocVatTu dvt;
  @override
  State<EditDVT> createState() => _EditDVTState();
}

class _EditDVTState extends State<EditDVT> {
  late final _textEditingControllerMa;
  late final _textEditingControllerTen;
  late final _textEditingControllerSDK;
  late final _textEditingControllerSQD;
  late final _textEditingControllerHCSDK;
  late final _textEditingControllerHamLuong;
  late final _textEditingControllerDongGoi;
  late final _textEditingControllerHangSX;
  late final _textEditingControllerNuocSX;
  late IconData icon;
  @override
  void initState() {
    // TODO: implement initState
    _textEditingControllerMa = TextEditingController(text: widget.dvt.ma);
    _textEditingControllerTen = TextEditingController(text: widget.dvt.ten);
    _textEditingControllerSDK = TextEditingController(text: widget.dvt.sdk);
    _textEditingControllerSQD = TextEditingController(text: widget.dvt.sqd);
    _textEditingControllerHCSDK = TextEditingController(text: widget.dvt.hcsdk);
    _textEditingControllerHamLuong =
        TextEditingController(text: widget.dvt.hamluong);
    _textEditingControllerDongGoi =
        TextEditingController(text: widget.dvt.donggoi);
    _textEditingControllerHangSX =
        TextEditingController(text: widget.dvt.hangsx);
    _textEditingControllerNuocSX =
        TextEditingController(text: widget.dvt.nuocsx);
    icon =
        widget.dvt.sudung! ? Icons.check_box_outlined : Icons.crop_square_sharp;
  }

  @override
  Widget build(BuildContext context) {
    void saveDataDVT() async {
      String ma = _textEditingControllerMa.text;
      String ten = _textEditingControllerTen.text;
      String sdk = _textEditingControllerSDK.text;
      String sqd = _textEditingControllerSQD.text;
      String hcsdk = _textEditingControllerHCSDK.text;
      String hamluong = _textEditingControllerHamLuong.text;
      String donggoi = _textEditingControllerDongGoi.text;
      String hangsx = _textEditingControllerHangSX.text;
      String nuocsx = _textEditingControllerNuocSX.text;
      bool? sudung = widget.dvt.sudung;
      DuocVatTu dvt = DuocVatTu(
          ma: ma,
          ten: ten,
          sdk: sdk,
          sqd: sqd,
          hcsdk: hcsdk,
          hamluong: hamluong,
          hoatchat: widget.dvt.hoatchat,
          duongdung: widget.dvt.duongdung,
          dvcapphat: widget.dvt.dvcapphat,
          donggoi: donggoi,
          hangsx: hangsx,
          nuocsx: nuocsx,
          sudung: sudung,
          id: '1');
      try {
        String url =
            'https://64d2e2e667b2662bf3db7e0a.mockapi.io/api/test/vattu/';
        final result = await BaseAPI().updateStateSVT(url, widget.dvt.id!, dvt);
        Navigator.of(context).pop('refresh');
      } catch (e) {
        print('=================\nco loi update dvt: ' + e.toString());
      }
      // print(sudung.toString());
    }

    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        backgroundColor: const Color(0xFFEEEEEE),
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: Color(0xFF6F9BD4),
          title: const Text(
            'Sửa thông tin dược vật tư',
            style: TextStyle(color: Colors.white, fontSize: 16),
          ),
        ),
        body: Scaffold(
          bottomNavigationBar: Container(
            // padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
            margin: EdgeInsets.fromLTRB(10, 10, 10, 5),
            height: 35,
            child: CustomElevatedButton(
                onClickIcon: () {
                  saveDataDVT();
                },
                text: 'Lưu',
                icon: Icons.save_alt,
                color: Color(0xFF6F9BD4),
                colorText: Colors.white),
          ),
          body: SingleChildScrollView(
            child: Container(
                margin: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(8),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 2,
                      blurRadius: 2,
                      offset: Offset(0, 2), // changes position of shadow
                    ),
                  ],
                ),
                child: Column(
                  children: [
                    Container(
                        margin: EdgeInsets.only(top: 10),
                        child: LineTextBorder(
                          title: 'Mã (*)',
                          value: widget.dvt.ma!,
                          textEditingController: _textEditingControllerMa,
                        )),
                    LineTextBorder(
                        title: 'Tên (*)',
                        value: widget.dvt.ten!,
                        textEditingController: _textEditingControllerTen),
                    LineTextBorder(
                        title: 'Số đăng ký',
                        value: widget.dvt.sdk!,
                        textEditingController: _textEditingControllerSDK),
                    LineTextBorder(
                        title: 'Số quyết định',
                        value: widget.dvt.sqd!,
                        textEditingController: _textEditingControllerSQD),
                    LineTextBorder(
                        title: 'Hoạt chất số đăng ký',
                        value: widget.dvt.hcsdk!,
                        textEditingController: _textEditingControllerHCSDK),
                    LineTextBorder(
                        title: 'Hàm lượng',
                        value: widget.dvt.hamluong!,
                        textEditingController: _textEditingControllerHamLuong),
                    EditDVTItem(
                      title: 'Hoạt chất',
                      value: widget.dvt.hoatchat!,
                      keyData: 'HoatChat',
                      returnData: (value) {
                        setState(() {
                          widget.dvt.hoatchat = value;
                        });
                      },
                    ),
                    EditDVTItem(
                      title: 'Đường dùng',
                      value: widget.dvt.duongdung!,
                      keyData: 'DuongDung',
                      returnData: (value) {
                        setState(() {
                          widget.dvt.duongdung = value;
                        });
                      },
                    ),
                    EditDVTItem(
                      title: 'Đơn vị cấp phát',
                      value: widget.dvt.dvcapphat!,
                      keyData: 'DonViCapPhat',
                      returnData: (value) {
                        setState(() {
                          widget.dvt.dvcapphat = value;
                        });
                      },
                    ),
                    LineTextBorder(
                        title: 'Đóng gói (*)',
                        value: widget.dvt.donggoi!,
                        textEditingController: _textEditingControllerDongGoi),
                    LineTextBorder(
                        title: 'Hãng sản xuất (*)',
                        value: widget.dvt.hangsx!,
                        textEditingController: _textEditingControllerHangSX),
                    LineTextBorder(
                        title: 'Nước sản xuất (*)',
                        value: widget.dvt.nuocsx!,
                        textEditingController: _textEditingControllerNuocSX),
                    Container(
                      margin: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            flex: 5,
                            child: Container(
                                child: Text('Sử dụng',
                                    style: TextStyle(fontSize: 12))),
                          ),
                          Expanded(
                              flex: 5,
                              child: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    icon = (icon == Icons.check_box_outlined)
                                        ? Icons.crop_square_sharp
                                        : Icons.check_box_outlined;
                                    widget.dvt.sudung = !widget.dvt.sudung!;
                                  });
                                },
                                child: Container(
                                    alignment: Alignment.centerLeft,
                                    child: Icon(
                                      icon,
                                      color: Colors.blue,
                                    )),
                              )),
                        ],
                      ),
                    ),
                  ],
                )),
          ),
        ),
      ),
    );
  }
}
