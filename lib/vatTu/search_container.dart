import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
// import 'package:test_item/item/box_search.dart';
// import 'package:test_item/item/custom_button.dart';

import '../item/box_search.dart';
import '../item/custom_button.dart';
import '../item/itemInputSearch.dart';
import 'add_dvt.dart';

class ListSearchItemVatTu extends StatefulWidget {
  const ListSearchItemVatTu(
      {required this.onClickAdd, required this.onClickSearch, super.key});
  final Function(String) onClickAdd;
  final Function(List<String>) onClickSearch;
  @override
  State<ListSearchItemVatTu> createState() => _ListSearchItemVatTuState();
}

class _ListSearchItemVatTuState extends State<ListSearchItemVatTu> {
  final _focusTgianMa = FocusNode();
  final _focusTgianTen = FocusNode();
  final _focusHoatChat = FocusNode();
  final _focusTT = FocusNode();

  final _textEdittingMa = TextEditingController();
  final _textEdittingTen = TextEditingController();
  final _textEdittingHoatChat = TextEditingController();
  final _textEdittingTT = TextEditingController();
  @override
  void initState() {
    super.initState();
    _focusTgianMa.addListener(_onFocusChange);
    _focusHoatChat.addListener(_onFocusChange);
    _focusTT.addListener(_onFocusChange);
    _focusTgianTen.addListener(_onFocusChange);
  }

  List<String> result = ['null', 'null', 'null', 'null'];
  String dataFromChild = '';
  void _onFocusChange() {
    setState(() {
      result[0] = _textEdittingMa.text;
      result[1] = _textEdittingTen.text;
      result[2] = _textEdittingHoatChat.text;
      result[3] = _textEdittingTT.text;
    });
  }

  @override
  void dispose() {
    super.dispose();
    _focusTgianMa.dispose();
    _focusHoatChat.dispose();
    _focusTT.dispose();
    _focusTgianTen.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(15, 10, 15, 10),
      decoration: BoxDecorationSearch,
      child: Column(
        children: [
          Row(
            children: <Widget>[
              Expanded(
                child: ItemSearchInput(
                  title: 'Mã',
                  focusNode: _focusTgianMa,
                  textEditingController: _textEdittingMa,
                ),
              ),
              Expanded(
                  child: ItemSearchInput(
                title: 'Tên',
                focusNode: _focusTgianTen,
                textEditingController: _textEdittingTen,
              )),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                  child: ItemSearchInput(
                title: 'Hoạt chất',
                focusNode: _focusHoatChat,
                textEditingController: _textEdittingHoatChat,
              )),
              Expanded(
                  child: ItemSearchInput(
                title: 'Trạng thái',
                focusNode: _focusTT,
                textEditingController: _textEdittingTT,
              )),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              CustomButton(
                text: 'Thêm mới',
                icon: Icons.add_circle_outline_outlined,
                onClick: () {
                  print('goi onclick');
                  Navigator.push(context,
                          MaterialPageRoute(builder: (context) => AddDVT()))
                      .then((value) {
                    widget.onClickAdd(value);
                  });
                  // widget.onClickAdd();
                },
              ),
              CustomButton(
                text: 'Tìm kiếm',
                icon: Icons.search_sharp,
                onClick: () {
                  widget.onClickSearch(result);
                },
              ),
            ],
          ),
          SizedBox(
            height: 10,
          )
        ],
      ),
    );
  }
}
