import 'package:flutter/material.dart';

import '../dialog/delete_dialog.dart';
import '../item/box_decoration_container.dart';
import '../item/elevated_button.dart';
import '../item/line_text.dart';
import '../model/dvt.dart';
import 'edit_DVT.dart';

class ChiTietDuocVatTu extends StatelessWidget {
  const ChiTietDuocVatTu({required this.dvt, super.key});
  final DuocVatTu dvt;
  @override
  Widget build(BuildContext context) {
    bool sd = dvt.sudung!;
    return Scaffold(
      backgroundColor: const Color(0xFFEEEEEE),
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: Color(0xFF6F9BD4),
        title: const Text(
          'Chi tiết thông tin dược vật tư',
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
      ),
      body: Scaffold(
        bottomNavigationBar: Container(
            height: 40,
            padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                CustomElevatedButton(
                    onClickIcon: () {
                      showDialog(
                          context: context,
                          builder: (context) {
                            return AlertPopup(
                              id: dvt.id!,
                              url:
                                  'https://64d2e2e667b2662bf3db7e0a.mockapi.io/api/test/vattu/',
                            );
                          }).then((value) {
                        Navigator.of(context).pop(value);
                      });
                    },
                    text: 'Xóa',
                    icon: Icons.save_alt,
                    color: Colors.white,
                    colorText: Color(0xFF6F9BD4)),
                CustomElevatedButton(
                    onClickIcon: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => EditDVT(
                                    dvt: dvt,
                                  ))).then((value) {
                        Navigator.of(context).pop(value);
                      });
                    },
                    text: 'Sửa',
                    icon: Icons.edit_note_outlined,
                    color: Color(0xFF6F9BD4),
                    colorText: Colors.white),
              ],
            )),
        body: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                        margin: const EdgeInsets.all(15),
                        decoration: BoxecorationContainer,
                        child: Column(
                          children: [
                            MyWidget(title: 'Mã (*)', text: dvt.ma!),
                            MyWidget(title: 'Tên (*)', text: dvt.ten!),
                            MyWidget(title: 'Số đăng ký', text: dvt.sdk!),
                            MyWidget(title: 'Số quyết định', text: dvt.sqd!),
                            MyWidget(title: 'Hoạt chất SDK', text: dvt.hcsdk!),
                            MyWidget(title: 'Hàm lượng', text: dvt.hamluong!),
                            MyWidget(title: 'Hoạt chất', text: dvt.hoatchat!),
                            MyWidget(title: 'Đường dùng', text: dvt.duongdung!),
                            MyWidget(
                                title: 'Đơn vị cấp phát', text: dvt.dvcapphat!),
                            MyWidget(title: 'Đóng gói (*)', text: dvt.donggoi!),
                            MyWidget(
                                title: 'Hãng sản xuất (*)', text: dvt.hangsx!),
                            MyWidget(title: 'Nước sản xuất', text: dvt.nuocsx!),
                            Container(
                              margin: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Expanded(
                                    flex: 5,
                                    child: Container(
                                        child: Text('Sử dụng',
                                            style: TextStyle(fontSize: 12))),
                                  ),
                                  Expanded(
                                      flex: 4,
                                      child: GestureDetector(
                                        onTap: () {},
                                        child: Container(
                                            alignment: Alignment.centerLeft,
                                            child: Icon(
                                              sd
                                                  ? Icons.check_box_outlined
                                                  : Icons.crop_square_sharp,
                                              color: Colors.blue,
                                            )),
                                      )),
                                ],
                              ),
                            ),
                          ],
                        )),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
