import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../login.dart';
import '../model/schedule_detail.dart';
import '../model/timeAppointment.dart';
import 'dialogTime.dart';
import 'directory.dart';
import 'edit_schedule.dart';
import 'package:http/http.dart' as http;

class LineInput extends StatefulWidget {
  const LineInput(
      {required this.title,
      required this.type,
      required this.index,
      super.key});
  final String title;
  final String type;
  final int index;

  @override
  State<LineInput> createState() => _LineInputState();
}

class _LineInputState extends State<LineInput> {
  DateTime _dateTime = DateTime.now();
  // Map<int, String> defaultValues = {
  //   1: 'Qua VNCare',
  //   2: 'Viện phí',
  //   3: 'Sinh Viên',
  //   4: 'Nam',
  //   5: 'Kinh',
  //   6: 'Việt Nam',
  //   7: 'Thái Bình',
  //   8: 'Thái Bình',
  //   9: 'Quỳnh Phụ',
  //   10: 'Quỳnh Trang',
  //   11: 'Khám sản',
  //   12: 'Phòng khám 1',
  //   13: 'BS Minh Phương',
  //   15: '09-07-2009',
  //   21: '09-08-2023 09:00'
  // };
  Map<int, String> defaultValues = {};
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getDataEdit('86816');
  }

  final _textEditingController = TextEditingController();
  Map<String, bool> stateTimePicker = {};

  //gọi api lấy giá trị hiện tại từng thông tin để sửa
  void getDataEdit(String index) async {
    // String token =
    //     'eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiIwMTAwOS5RTERMSyIsImhpc0lkIjoxNDMzMSwic05hbWUiOiJRdeG6o24gbMO9IMSR4bq3dCBs4buLY2gga2jDoW0iLCJyb2xlcyI6WyJST0xFX1VTRVIiXSwiaXNzIjoic3lzLXFsaGstYXBpIiwiZnVsbE5hbWUiOiJRdeG6o24gbMO9IMSR4bq3dCBs4buLY2gga2jDoW0iLCJob3NwaXRhbE5hbWUiOiJC4buHbmggdmnhu4duIELGsHUgxJFp4buHbiAoQuG7mSBCxrB1IENow61uaCB2LnRow7RuZykiLCJ1c2VySUQiOjQzMzQ4LCJoTmFtZSI6IkLhu4duaCB2aeG7h24gQsawdSDEkWnhu4duIChC4buZIELGsHUgQ2jDrW5oIHYudGjDtG5nKSIsImhvc3BpdGFsQ29kZSI6IjAxMDA5IiwidXNlckdyb3VwSWQiOjMsImV4cCI6MTY4OTkwMzYyMywiaWF0IjoxNjg5OTAwMDIzLCJ1c2VyTGV2ZWxJZCI6MywianRpIjoiYmQ3MGU2NjQtN2MzYS00ZjVhLWEwYzctMDNkOWZhODY5NWM4IiwidXNlcm5hbWUiOiIwMTAwOS5RTERMSyJ9.fbuCs5qysqhwAPDFnOC_1cJnFyIXgN6wLcf5MR7b-ccPCwq2DRfL3mAo80zZ3kehTQki358gXk0a7RdZIyDs7DwOKmxB0RBKO2TJyiDuvz6MD51oXUFw82CFVDuP-K3nF1wu6LYAXcCVA63GXsr9vOhFdizWbBXRLMoBm0kHWdGNR0pfa3ZKLmx_JeWKFqFgXKYmnCzUMITY3h4mxdkKBUugYMpfrYt0bBp4kR4kIhCiS-RaZG7QWx3PxE2itTr75omlhBswwe1L791JoQLcQrAyWvH3Mi_TeZJdbNBYj9n82La_X2yIUciqXqnFv_tQs2Nmy-D7HFkA8Bmnsd5HAmnuAjf5H3R13Cj9MqtLUXt_2vuSFOAzTmWHJqeWICv7mSTFkkhsxYAxnaqlrzT_pzSYyvFD6tP26T7vWVNPZ16yhviswePEIkjGFTaMMlNmdM5oTccVZvTh7Tq6AxPE7F2PvU-7K2b1I8w9UL__JQOGt9mw6revVZ4uhbIN52dNqu-8DZaJIOPlcB6TREg90bWXDwB3ycevESVmrdbr8uAz4lYcMm0hC-Omytcpymzmbg2VETTsAT5CdZgEHOp6zLcF1oXcwRLJghNl75exHY-5VJcY7AiISSSgTfIZY_GItFJtvImnot7qt-0hKjgjaDDj0kdIgHzbCBqwSqr3o1M';
    String token = SharedPrefs().token;
    String url =
        'https://staging-apidatlichkham.vncare.vn/patient_api/api/QlhkMobile/v1.3.0/business-service/chiTietLichHenKham?henKhamId=' +
            index;
    Map<String, String> header = {
      'Authorization': 'Bearer $token',
      'Content-Type': 'application/json',
      'Accept-Language': 'vi',
    };
    print('goi api============================');
    http.get(Uri.parse(url), headers: header).then((http.Response response) {
      print('co tra ve');
      final String jsonBody = utf8.decode(response.bodyBytes);
      final int statusCode = response.statusCode;
      if (statusCode != 200) {
        print('statuscode: $statusCode');
      } else {
        print('goi thanh cong');
        final JsonDecoder _decoder = new JsonDecoder();
        final resultList = _decoder.convert(jsonBody);
        final resultResponse = resultList['result'];
        final t = Schedule.fromJson(resultResponse);
        print('lay duoc result');
        setState(() {
          defaultValues[1] = t.kenhDangKy.toString();
          defaultValues[2] = t.doiTuongBenhNhan.toString();
          defaultValues[3] = t.ngheNghiep.toString();
          defaultValues[4] = t.gioiTinh.toString();
          defaultValues[5] = t.danToc.toString();
          defaultValues[6] = t.quocTich.toString();
          defaultValues[7] = t.tinhKhaiSinh.toString();
          defaultValues[8] = t.tinhKhaiSinh.toString();
          defaultValues[9] = t.huyen.toString();
          defaultValues[10] = t.xa.toString();
          defaultValues[11] = t.yeuCauKham.toString();
          defaultValues[12] = t.phongKham.toString();
          defaultValues[13] = t.bacSi.toString();
          defaultValues[15] = t.ngaySinh.toString();
          defaultValues[21] = t.thoiGianDenKham.toString();
        });
      }
    });
  }

  void showDataAlert(context, int indexns) {
    // getDataHour();
    showDialog(
        context: context,
        builder: (context) {
          return Container(
            padding: EdgeInsets.all(20),
            child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                ),
                child: ShowDialog(
                  indexns: indexns,
                )),
          );
        }).then((value) {
      setState(() {
        defaultValues[widget.index] = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    print('vao build --------------------------------------------');
    void transfer() async {
      final result = await Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => Directorys(index: widget.index)));
      setState(() {
        defaultValues[widget.index] = result;
      });
    }

    if (widget.type == "input") {
      return Row(
        children: [
          Container(
              margin: const EdgeInsets.fromLTRB(10, 5, 0, 5),
              alignment: Alignment.centerLeft,
              width: 150,
              height: 25,
              child: Text(widget.title)),
          Container(
              alignment: Alignment.centerLeft,
              width: 175,
              height: 25,
              child: TextFormField(
                controller: _textEditingController,
                decoration: const InputDecoration(
                    focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(width: 0.5, color: Colors.black)),
                    fillColor: Colors.blue,
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        borderSide:
                            BorderSide(color: Colors.grey, width: 0.5))),
              )),
        ],
      );
    } else if (widget.type == 'dropbutton') {
      return Container(
        child: Row(
          children: [
            Container(
                margin: const EdgeInsets.fromLTRB(10, 5, 0, 15),
                alignment: Alignment.centerLeft,
                width: 150,
                height: 25,
                child: Text(widget.title)),
            Container(
              alignment: Alignment.centerLeft,
              // padding: EdgeInsets.all(5),
              width: 175,
              height: 25,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(color: Colors.grey, width: 0.5)),
              //onclick vao
              child: GestureDetector(
                onTap: () {
                  if (widget.index == 21) {
                    showDataAlert(context, widget.index);
                  } else if (widget.index == 15) {
                    showDatePicker(
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime(1920),
                            lastDate: DateTime(2024))
                        .then((value) {
                      setState(() {
                        _dateTime = value!;
                        defaultValues[widget.index] =
                            _dateTime.toString().substring(0, 10);
                      });
                    });
                  } else {
                    transfer();
                  }
                },
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: 140,
                        child: Text(
                          '   ${defaultValues[widget.index].toString()}',
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Container(
                        height: 21,
                        margin: const EdgeInsets.only(right: 5),
                        child: widget.title == 'Ngày sinh (*)' ||
                                widget.title == 'Thời gian khám (*)'
                            ? Icon(Icons.calendar_month_outlined)
                            : const Icon(Icons.arrow_drop_down),
                      )
                      // const Icon(Icons.arrow_drop_down)
                    ]),
              ),
            ),
          ],
        ),
      );
    } else {
      return const Text('Ngoại lệ');
    }
  }
}
