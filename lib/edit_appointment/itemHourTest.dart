import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:product_layout_app/model/timeAppointment.dart';
import 'package:http/http.dart' as http;

class ItemGridViewTest extends StatefulWidget {
  ItemGridViewTest(
      {required this.onItemClick,
      required this.chotrong,
      required this.stateTimePicker,
      required this.mess,
      super.key});
  final Function(String) onItemClick;
  final List<String> chotrong;
  final Map<String, bool> stateTimePicker;
  final String mess;
  @override
  State<ItemGridViewTest> createState() => _ItemGridViewTestState();
}

class _ItemGridViewTestState extends State<ItemGridViewTest> {
  String itemPickedHour = '';
  Color _colorBackGround = Colors.white;
  Color _colorText = Colors.blue;
  @override
  Widget build(BuildContext context) {
    if (widget.chotrong.length == 0) {
      if (widget.mess == 'loaderr') {
        return Container(
          height: 120,
          margin: EdgeInsets.all(5),
          child: Center(child: Text('Không thể lấy dữ liệu')),
        );
      } else if (widget.mess == 'outtime') {
        return Container(
          height: 120,
          margin: EdgeInsets.all(5),
          child: Center(child: Text('Không còn thời gian khám')),
        );
      } else  {
        return Container(
          height: 120,
          margin: EdgeInsets.all(5),
          child: Center(child: CircularProgressIndicator()),
        );
      }
    } else {
      return Container(
        height: 120,
        margin: const EdgeInsets.all(5),
        child: GridView.builder(
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
            childAspectRatio: 2 / 1,
          ),
          itemCount: widget.chotrong.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              margin: const EdgeInsets.all(5),
              child: GestureDetector(
                onTap: () {
                  itemPickedHour = widget.chotrong[index];
                  widget.onItemClick(itemPickedHour.toString());
                  setState(() {
                    for (int i = 0; i < widget.chotrong.length; i++) {
                      if (i == index) {
                        widget.stateTimePicker[widget.chotrong[i]] = true;
                      } else {
                        widget.stateTimePicker[widget.chotrong[i]] = false;
                      }
                    }
                  });
                },
                child: Card(
                  color: widget.stateTimePicker[widget.chotrong[index]] == true
                      ? Colors.blue
                      : Colors.white,
                  shape: RoundedRectangleBorder(
                    side: const BorderSide(color: Colors.blue, width: 1),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Center(
                    child: Text(
                      widget.chotrong[index],
                      style: TextStyle(
                        color: widget.stateTimePicker[widget.chotrong[index]] ==
                                true
                            ? Colors.white
                            : Colors.blue,
                      ),
                    ),
                  ),
                ),
              ),
            );
          },
        ),
      );
    }
  }
}
