import 'dart:convert';
import 'dart:ffi';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:product_layout_app/edit_appointment/alert_dialog.dart';
import 'package:table_calendar/table_calendar.dart';
import '../login.dart';
import '../model/timeAppointment.dart';
import 'itemHourTest.dart';
import 'package:http/http.dart' as http;


class ShowDialog extends StatefulWidget {
  const ShowDialog({required this.indexns, super.key});
  final int indexns;
  // final Map<String, bool> stateHour;
  @override
  State<ShowDialog> createState() => _ShowDialogState();
}

class _ShowDialogState extends State<ShowDialog> {
  DateTime? _selectedDay;
  DateTime _focusDay = DateTime.now();
  String? timePicked = '';
  late List<String> chotrong = [];
  late Map<String, bool> stateTimePicker = {};
  String message = 'ok';
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    List<String> tmp = DateTime.now().toString().substring(0, 10).split('-');
    getDataHour(tmp[0] + tmp[1] + tmp[2]);
  }

  //api
  void getDataHour(String day) async {
    // String token =
    //     'eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiIwMTAwOS5RTERMSyIsImhpc0lkIjoxNDMzMSwic05hbWUiOiJRdeG6o24gbMO9IMSR4bq3dCBs4buLY2gga2jDoW0iLCJyb2xlcyI6WyJST0xFX1VTRVIiXSwiaXNzIjoic3lzLXFsaGstYXBpIiwiZnVsbE5hbWUiOiJRdeG6o24gbMO9IMSR4bq3dCBs4buLY2gga2jDoW0iLCJob3NwaXRhbE5hbWUiOiJC4buHbmggdmnhu4duIELGsHUgxJFp4buHbiAoQuG7mSBCxrB1IENow61uaCB2LnRow7RuZykiLCJ1c2VySUQiOjQzMzQ4LCJoTmFtZSI6IkLhu4duaCB2aeG7h24gQsawdSDEkWnhu4duIChC4buZIELGsHUgQ2jDrW5oIHYudGjDtG5nKSIsImhvc3BpdGFsQ29kZSI6IjAxMDA5IiwidXNlckdyb3VwSWQiOjMsImV4cCI6MTY4OTkwMzYyMywiaWF0IjoxNjg5OTAwMDIzLCJ1c2VyTGV2ZWxJZCI6MywianRpIjoiYmQ3MGU2NjQtN2MzYS00ZjVhLWEwYzctMDNkOWZhODY5NWM4IiwidXNlcm5hbWUiOiIwMTAwOS5RTERMSyJ9.fbuCs5qysqhwAPDFnOC_1cJnFyIXgN6wLcf5MR7b-ccPCwq2DRfL3mAo80zZ3kehTQki358gXk0a7RdZIyDs7DwOKmxB0RBKO2TJyiDuvz6MD51oXUFw82CFVDuP-K3nF1wu6LYAXcCVA63GXsr9vOhFdizWbBXRLMoBm0kHWdGNR0pfa3ZKLmx_JeWKFqFgXKYmnCzUMITY3h4mxdkKBUugYMpfrYt0bBp4kR4kIhCiS-RaZG7QWx3PxE2itTr75omlhBswwe1L791JoQLcQrAyWvH3Mi_TeZJdbNBYj9n82La_X2yIUciqXqnFv_tQs2Nmy-D7HFkA8Bmnsd5HAmnuAjf5H3R13Cj9MqtLUXt_2vuSFOAzTmWHJqeWICv7mSTFkkhsxYAxnaqlrzT_pzSYyvFD6tP26T7vWVNPZ16yhviswePEIkjGFTaMMlNmdM5oTccVZvTh7Tq6AxPE7F2PvU-7K2b1I8w9UL__JQOGt9mw6revVZ4uhbIN52dNqu-8DZaJIOPlcB6TREg90bWXDwB3ycevESVmrdbr8uAz4lYcMm0hC-Omytcpymzmbg2VETTsAT5CdZgEHOp6zLcF1oXcwRLJghNl75exHY-5VJcY7AiISSSgTfIZY_GItFJtvImnot7qt-0hKjgjaDDj0kdIgHzbCBqwSqr3o1M';
    String token = SharedPrefs().token;
    String url =
        'https://staging-apidatlichkham.vncare.vn/patient_api/api/QlhkMobile/v1.3.0/business-service/danhSachBlockOfflineCsyt?ngayKham=' +
            day +
            '&maPhongKham=46935&maBacSi=';
    Map<String, String> header = {
      'Authorization': 'Bearer $token',
      'Content-Type': 'application/json',
      'Accept-Language': 'vi',
    };
    print('url: $url');
    http.get(Uri.parse(url), headers: header).then((http.Response response) {
      final String jsonBody = response.body;
      final int statusCode = response.statusCode;
      if (statusCode != 200) {
        setState(() {
          message = 'loaderr';
        });
      } else {
        final JsonDecoder _decoder = new JsonDecoder();
        final resultList = _decoder.convert(jsonBody);
        final List resultResponse = resultList['result'];
        List<TimeBlock> t = resultResponse
            .map((contactRaw) => TimeBlock.fromJson(contactRaw))
            .toList();
        if (t.length == 0) {
          setState(() {
            message = 'outtime';
          });
        } else {
          for (int i = 0; i < t.length; i++) {
            setState(() {
              stateTimePicker[t[i].tHOIGIANKHAM.toString()] = false;
            });
          }
          print('state time picker length: ${stateTimePicker.length}');
          print(stateTimePicker.keys.toList());
          setState(() {
            chotrong = stateTimePicker.keys.toList();
          });
        }
      }
    });
  }

  //test get api dung chung

  void checkTimePicker() {
    //KIEM TRA GIO DA DUOC CHON CHUA
    if (timePicked.toString().trim() == '') {
      showDialog(
          context: context,
          builder: (context) {
            return AlertPopup();
          });
    } else {
      String backValues =
          _focusDay.toString().substring(0, 10) + " " + timePicked.toString();
      Navigator.pop(context, backValues);
    }
  }

  var _calendarFormat = CalendarFormat.month;

  @override
  Widget build(BuildContext context) {
    print('goi build==============================================');
    return MaterialApp(
      home: Scaffold(
        body: SafeArea(
            child: Container(
          child: SingleChildScrollView(
            child: Column(
              children: [
                //icon cancel
                Container(
                  margin: EdgeInsets.fromLTRB(0, 20, 20, 0),
                  alignment: Alignment.topRight,
                  child: GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Icon(Icons.cancel_rounded)),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(25, 0, 25, 10),
                  //tablecalendar
                  child: TableCalendar(
                    locale: "en_US",
                    rowHeight: 33,
                    headerStyle: const HeaderStyle(
                        formatButtonVisible: false, titleCentered: true),
                    availableGestures: AvailableGestures.all,
                    focusedDay: _focusDay,
                    firstDay: DateTime.utc(2000, 10, 16),
                    lastDay: DateTime.utc(2024, 3, 14),
                    selectedDayPredicate: (day) {
                      return isSameDay(_selectedDay, day);
                    },
                    onDaySelected: (selectedDay, focusDay) {
                      setState(() {
                        _selectedDay = selectedDay;
                        _focusDay = focusDay;
                        chotrong.clear();
                        stateTimePicker.clear();
                        message = 'ok';
                      });
                      List<String> dayy =
                          _selectedDay.toString().substring(0, 10).split('-');
                      String day = dayy[0] + dayy[1] + dayy[2];
                      getDataHour(day);
                    },
                    calendarFormat: _calendarFormat,
                    onFormatChanged: (format) {
                      setState(() {
                        _calendarFormat = format;
                      });
                    },
                    onPageChanged: (focusedDay) {
                      _focusDay = focusedDay;
                    },
                  ),
                ),
                //time
                Container(
                  padding: const EdgeInsets.fromLTRB(25, 5, 25, 0),
                  width: MediaQuery.of(context).size.width,
                  child: Card(
                    shape: RoundedRectangleBorder(
                      side:
                          const BorderSide(color: Color(0xFF6F9BD4), width: 1),
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Container(
                            height: 30,
                            decoration: const BoxDecoration(
                                color: Color(0xFF6F9BD4),
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(15),
                                    topRight: Radius.circular(15))),
                            alignment: Alignment.center,
                            width: MediaQuery.of(context).size.width,
                            child: const Text(
                              'Số chỗ còn trống',
                              style:
                                  TextStyle(fontSize: 15, color: Colors.white),
                            ),
                          ),
                          ItemGridViewTest(
                            onItemClick: (String value) {
                              timePicked = value;
                            },
                            chotrong: chotrong,
                            stateTimePicker: stateTimePicker,
                            mess: message,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                //tt
                Container(
                    height: 85,
                    margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    padding: const EdgeInsets.all(20),
                    width: MediaQuery.of(context).size.width,
                    child: GestureDetector(
                      onTap: () {
                        checkTimePicker();
                      },
                      child: Card(
                        color: const Color(0xFF6F9BD4),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        child: const Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.arrow_circle_right_outlined,
                              color: Colors.white,
                            ),
                            Center(
                                child: Text(
                              ' Tiếp tục',
                              style: TextStyle(color: Colors.white),
                            )),
                          ],
                        ),
                      ),
                    ))
              ],
            ),
          ),
        )),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}
