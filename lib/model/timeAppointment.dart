class TimeBlock {
  String? iDBLOCK;
  String? tHOIGIANKHAM;
  String? sTT;

  TimeBlock({this.iDBLOCK, this.tHOIGIANKHAM, this.sTT});

  TimeBlock.fromJson(Map<String, dynamic> json) {
    iDBLOCK = json['IDBLOCK'];
    tHOIGIANKHAM = json['THOIGIANKHAM'];
    sTT = json['STT'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['IDBLOCK'] = this.iDBLOCK;
    data['THOIGIANKHAM'] = this.tHOIGIANKHAM;
    data['STT'] = this.sTT;
    return data;
  }
}
