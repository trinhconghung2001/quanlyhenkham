class Schedule {
  String? henKhamId;
  String? eButtonCsytSua;
  String? eButtonCsytXacNhan;
  String? eButtonCsytTuChoi;
  String? eButtonTvtxSua;
  String? eButtonTvtxXacNhan;
  String? eButtonTvtxTuChoi;
  String? eButtonTvtxTraKq;
  String? maTrangThaiLichHen;
  String? trangThaiLichHen;
  String? maTrangThaiDenKham;
  String? trangThaiDenKham;
  String? hoTen;
  String? maKenhDangKy;
  String? kenhDangKy;
  String? ngaySinh;
  String? maNgheNghiep;
  String? ngheNghiep;
  String? maGioiTinh;
  String? gioiTinh;
  String? maDanToc;
  String? danToc;
  String? maQuocTich;
  String? quocTich;
  String? maTinhKhaiSinh;
  String? tinhKhaiSinh;
  String? sdt;
  String? cmt;
  String? email;
  String? maTinhTp;
  String? tinhTp;
  String? maHuyen;
  String? huyen;
  String? maXa;
  String? xa;
  String? soNha;
  String? maDoiTuongBenhNhan;
  String? doiTuongBenhNhan;
  String? soTheBhyt;
  String? giaTriTheTu;
  String? giaTriTheDen;
  String? maKcbBanDau;
  String? maVung;
  String? diaChiBhyt;
  String? maTuyen;
  String? tuyen;
  String? maYeuCauKham;
  String? yeuCauKham;
  String? maPhongKham;
  String? phongKham;
  String? maBacSi;
  String? bacSi;
  String? thoiGianDenKham;
  String? maBlockGio;
  String? noiDungKham;
  String? khamSangLoc;
  String? trieuChung;
  String? benhManTinh;
  String? ghiChu;
  String? nguoiXacNhan;
  String? nguoiTraKq;

  Schedule(
      {this.henKhamId,
      this.eButtonCsytSua,
      this.eButtonCsytXacNhan,
      this.eButtonCsytTuChoi,
      this.eButtonTvtxSua,
      this.eButtonTvtxXacNhan,
      this.eButtonTvtxTuChoi,
      this.eButtonTvtxTraKq,
      this.maTrangThaiLichHen,
      this.trangThaiLichHen,
      this.maTrangThaiDenKham,
      this.trangThaiDenKham,
      this.hoTen,
      this.maKenhDangKy,
      this.kenhDangKy,
      this.ngaySinh,
      this.maNgheNghiep,
      this.ngheNghiep,
      this.maGioiTinh,
      this.gioiTinh,
      this.maDanToc,
      this.danToc,
      this.maQuocTich,
      this.quocTich,
      this.maTinhKhaiSinh,
      this.tinhKhaiSinh,
      this.sdt,
      this.cmt,
      this.email,
      this.maTinhTp,
      this.tinhTp,
      this.maHuyen,
      this.huyen,
      this.maXa,
      this.xa,
      this.soNha,
      this.maDoiTuongBenhNhan,
      this.doiTuongBenhNhan,
      this.soTheBhyt,
      this.giaTriTheTu,
      this.giaTriTheDen,
      this.maKcbBanDau,
      this.maVung,
      this.diaChiBhyt,
      this.maTuyen,
      this.tuyen,
      this.maYeuCauKham,
      this.yeuCauKham,
      this.maPhongKham,
      this.phongKham,
      this.maBacSi,
      this.bacSi,
      this.thoiGianDenKham,
      this.maBlockGio,
      this.noiDungKham,
      this.khamSangLoc,
      this.trieuChung,
      this.benhManTinh,
      this.ghiChu,
      this.nguoiXacNhan,
      this.nguoiTraKq});

  Schedule.fromJson(Map<String, dynamic> json) {
    henKhamId = json['henKhamId'];
    eButtonCsytSua = json['eButtonCsytSua'];
    eButtonCsytXacNhan = json['eButtonCsytXacNhan'];
    eButtonCsytTuChoi = json['eButtonCsytTuChoi'];
    eButtonTvtxSua = json['eButtonTvtxSua'];
    eButtonTvtxXacNhan = json['eButtonTvtxXacNhan'];
    eButtonTvtxTuChoi = json['eButtonTvtxTuChoi'];
    eButtonTvtxTraKq = json['eButtonTvtxTraKq'];
    maTrangThaiLichHen = json['maTrangThaiLichHen'];
    trangThaiLichHen = json['trangThaiLichHen'];
    maTrangThaiDenKham = json['maTrangThaiDenKham'];
    trangThaiDenKham = json['trangThaiDenKham'];
    hoTen = json['hoTen'];
    maKenhDangKy = json['maKenhDangKy'];
    kenhDangKy = json['kenhDangKy'];
    ngaySinh = json['ngaySinh'];
    maNgheNghiep = json['maNgheNghiep'];
    ngheNghiep = json['ngheNghiep'];
    maGioiTinh = json['maGioiTinh'];
    gioiTinh = json['gioiTinh'];
    maDanToc = json['maDanToc'];
    danToc = json['danToc'];
    maQuocTich = json['maQuocTich'];
    quocTich = json['quocTich'];
    maTinhKhaiSinh = json['maTinhKhaiSinh'];
    tinhKhaiSinh = json['tinhKhaiSinh'];
    sdt = json['sdt'];
    cmt = json['cmt'];
    email = json['email'];
    maTinhTp = json['maTinhTp'];
    tinhTp = json['tinhTp'];
    maHuyen = json['maHuyen'];
    huyen = json['huyen'];
    maXa = json['maXa'];
    xa = json['xa'];
    soNha = json['soNha'];
    maDoiTuongBenhNhan = json['maDoiTuongBenhNhan'];
    doiTuongBenhNhan = json['doiTuongBenhNhan'];
    soTheBhyt = json['soTheBhyt'];
    giaTriTheTu = json['giaTriTheTu'];
    giaTriTheDen = json['giaTriTheDen'];
    maKcbBanDau = json['maKcbBanDau'];
    maVung = json['maVung'];
    diaChiBhyt = json['diaChiBhyt'];
    maTuyen = json['maTuyen'];
    tuyen = json['tuyen'];
    maYeuCauKham = json['maYeuCauKham'];
    yeuCauKham = json['yeuCauKham'];
    maPhongKham = json['maPhongKham'];
    phongKham = json['phongKham'];
    maBacSi = json['maBacSi'];
    bacSi = json['bacSi'];
    thoiGianDenKham = json['thoiGianDenKham'];
    maBlockGio = json['maBlockGio'];
    noiDungKham = json['noiDungKham'];
    khamSangLoc = json['khamSangLoc'];
    trieuChung = json['trieuChung'];
    benhManTinh = json['benhManTinh'];
    ghiChu = json['ghiChu'];
    nguoiXacNhan = json['nguoiXacNhan'];
    nguoiTraKq = json['nguoiTraKq'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['henKhamId'] = this.henKhamId;
    data['eButtonCsytSua'] = this.eButtonCsytSua;
    data['eButtonCsytXacNhan'] = this.eButtonCsytXacNhan;
    data['eButtonCsytTuChoi'] = this.eButtonCsytTuChoi;
    data['eButtonTvtxSua'] = this.eButtonTvtxSua;
    data['eButtonTvtxXacNhan'] = this.eButtonTvtxXacNhan;
    data['eButtonTvtxTuChoi'] = this.eButtonTvtxTuChoi;
    data['eButtonTvtxTraKq'] = this.eButtonTvtxTraKq;
    data['maTrangThaiLichHen'] = this.maTrangThaiLichHen;
    data['trangThaiLichHen'] = this.trangThaiLichHen;
    data['maTrangThaiDenKham'] = this.maTrangThaiDenKham;
    data['trangThaiDenKham'] = this.trangThaiDenKham;
    data['hoTen'] = this.hoTen;
    data['maKenhDangKy'] = this.maKenhDangKy;
    data['kenhDangKy'] = this.kenhDangKy;
    data['ngaySinh'] = this.ngaySinh;
    data['maNgheNghiep'] = this.maNgheNghiep;
    data['ngheNghiep'] = this.ngheNghiep;
    data['maGioiTinh'] = this.maGioiTinh;
    data['gioiTinh'] = this.gioiTinh;
    data['maDanToc'] = this.maDanToc;
    data['danToc'] = this.danToc;
    data['maQuocTich'] = this.maQuocTich;
    data['quocTich'] = this.quocTich;
    data['maTinhKhaiSinh'] = this.maTinhKhaiSinh;
    data['tinhKhaiSinh'] = this.tinhKhaiSinh;
    data['sdt'] = this.sdt;
    data['cmt'] = this.cmt;
    data['email'] = this.email;
    data['maTinhTp'] = this.maTinhTp;
    data['tinhTp'] = this.tinhTp;
    data['maHuyen'] = this.maHuyen;
    data['huyen'] = this.huyen;
    data['maXa'] = this.maXa;
    data['xa'] = this.xa;
    data['soNha'] = this.soNha;
    data['maDoiTuongBenhNhan'] = this.maDoiTuongBenhNhan;
    data['doiTuongBenhNhan'] = this.doiTuongBenhNhan;
    data['soTheBhyt'] = this.soTheBhyt;
    data['giaTriTheTu'] = this.giaTriTheTu;
    data['giaTriTheDen'] = this.giaTriTheDen;
    data['maKcbBanDau'] = this.maKcbBanDau;
    data['maVung'] = this.maVung;
    data['diaChiBhyt'] = this.diaChiBhyt;
    data['maTuyen'] = this.maTuyen;
    data['tuyen'] = this.tuyen;
    data['maYeuCauKham'] = this.maYeuCauKham;
    data['yeuCauKham'] = this.yeuCauKham;
    data['maPhongKham'] = this.maPhongKham;
    data['phongKham'] = this.phongKham;
    data['maBacSi'] = this.maBacSi;
    data['bacSi'] = this.bacSi;
    data['thoiGianDenKham'] = this.thoiGianDenKham;
    data['maBlockGio'] = this.maBlockGio;
    data['noiDungKham'] = this.noiDungKham;
    data['khamSangLoc'] = this.khamSangLoc;
    data['trieuChung'] = this.trieuChung;
    data['benhManTinh'] = this.benhManTinh;
    data['ghiChu'] = this.ghiChu;
    data['nguoiXacNhan'] = this.nguoiXacNhan;
    data['nguoiTraKq'] = this.nguoiTraKq;
    return data;
  }
}
