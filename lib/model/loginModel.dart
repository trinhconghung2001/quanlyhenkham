class Login {
    int? errorCode;
    String? errorMessage;
    Result? result;

    Login({this.errorCode, this.errorMessage, this.result});

    Login.fromJson(Map<String, dynamic> json) {
        errorCode = json["errorCode"];
        errorMessage = json["errorMessage"];
        result = json["result"] == null ? null : Result.fromJson(json["result"]);
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> _data = <String, dynamic>{};
        _data["errorCode"] = errorCode;
        _data["errorMessage"] = errorMessage;
        if(result != null) {
            _data["result"] = result?.toJson();
        }
        return _data;
    }
}

class Result {
    String? accessToken;
    String? tokenType;
    String? refreshToken;
    int? expiresIn;

    Result({this.accessToken, this.tokenType, this.refreshToken, this.expiresIn});

    Result.fromJson(Map<String, dynamic> json) {
        accessToken = json["access_token"];
        tokenType = json["token_type"];
        refreshToken = json["refresh_token"];
        expiresIn = json["expires_in"];
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> _data = <String, dynamic>{};
        _data["access_token"] = accessToken;
        _data["token_type"] = tokenType;
        _data["refresh_token"] = refreshToken;
        _data["expires_in"] = expiresIn;
        return _data;
    }
}