import 'dart:convert';

Notifications listNotificationsFromJson(String str) =>Notifications.fromJson(json.decode(str));
String listNotificationsToJson(Notifications data) => json.encode(data.toJson());

class Notifications {
    int? errorCode;
    String? errorMessage;
    Result? result;

    Notifications({this.errorCode, this.errorMessage, this.result});

    Notifications.fromJson(Map<String, dynamic> json) {
        errorCode = json["errorCode"];
        errorMessage = json["errorMessage"];
        result = json["result"] == null ? null : Result.fromJson(json["result"]);
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> _data = <String, dynamic>{};
        _data["errorCode"] = errorCode;
        _data["errorMessage"] = errorMessage;
        if(result != null) {
            _data["result"] = result?.toJson();
        }
        return _data;
    }
}

class Result {
    int? tongSo;
    int? soDong;
    List<DanhSach>? danhSach;

    Result({this.tongSo, this.soDong, this.danhSach});

    Result.fromJson(Map<String, dynamic> json) {
        tongSo = json["tongSo"];
        soDong = json["soDong"];
        danhSach = json["danhSach"] == null ? null : (json["danhSach"] as List).map((e) => DanhSach.fromJson(e)).toList();
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> _data = <String, dynamic>{};
        _data["tongSo"] = tongSo;
        _data["soDong"] = soDong;
        if(danhSach != null) {
            _data["danhSach"] = danhSach?.map((e) => e.toJson()).toList();
        }
        return _data;
    }
}

class DanhSach {
    String? notificationId;
    String? loai;
    String? createDate;
    Data? data;

    DanhSach({this.notificationId, this.loai, this.createDate, this.data});

    DanhSach.fromJson(Map<String, dynamic> json) {
        notificationId = json["notificationId"];
        loai = json["loai"];
        createDate = json["createDate"];
        data = json["data"] == null ? null : Data.fromJson(json["data"]);
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> _data = <String, dynamic>{};
        _data["notificationId"] = notificationId;
        _data["loai"] = loai;
        _data["createDate"] = createDate;
        if(data != null) {
            _data["data"] = data?.toJson();
        }
        return _data;
    }
}

class Data {
    String? title;
    String? henKhamId;
    String? tenBenhNhan;
    String? doiTuongBenhNhan;
    String? tenPhongKham;
    String? yeuCauKham;
    String? ngayGioHen;

    Data({this.title, this.henKhamId, this.tenBenhNhan, this.doiTuongBenhNhan, this.tenPhongKham, this.yeuCauKham, this.ngayGioHen});

    Data.fromJson(Map<String, dynamic> json) {
        title = json["title"];
        henKhamId = json["henKhamId"];
        tenBenhNhan = json["tenBenhNhan"];
        doiTuongBenhNhan = json["doiTuongBenhNhan"];
        tenPhongKham = json["tenPhongKham"];
        yeuCauKham = json["yeuCauKham"];
        ngayGioHen = json["ngayGioHen"];
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> _data = <String, dynamic>{};
        _data["title"] = title;
        _data["henKhamId"] = henKhamId;
        _data["tenBenhNhan"] = tenBenhNhan;
        _data["doiTuongBenhNhan"] = doiTuongBenhNhan;
        _data["tenPhongKham"] = tenPhongKham;
        _data["yeuCauKham"] = yeuCauKham;
        _data["ngayGioHen"] = ngayGioHen;
        return _data;
    }
}