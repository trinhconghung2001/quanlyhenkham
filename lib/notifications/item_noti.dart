// ignore_for_file: unused_local_variable

import 'package:flutter/material.dart';
import 'package:product_layout_app/componentWidget/rowflexWidget/RowTextItem.dart';
import 'package:product_layout_app/componentWidget/styles/CustomColor.dart';

class itemNotifi extends StatelessWidget {
  final String? tenbenhNhan;
  final String? tenphongKham;
  final String? lydo;
  final String? thoigianhen;
  final String? loailich;
  final String? thoigiandat;

  const itemNotifi({
    Key? key, 
    this.tenbenhNhan, 
    this.tenphongKham, 
    this.lydo, 
    this.thoigianhen, 
    this.loailich, 
    this.thoigiandat,
    
  }) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    double width_new = MediaQuery.of(context).size.width;
    double height_new = MediaQuery.of(context).size.height;

    return Padding(
      padding: const EdgeInsets.only(top: 10.0, left: 10, right: 10, bottom: 10),
    child : Container(
      height: height_new * 0.26,
      //width: 120.0,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(8)),
        boxShadow: [
          BoxShadow(
            color: Colors.black54,
            blurRadius: 8,
            spreadRadius: 0,
            offset: Offset(0.0, 3.0)
          )
        ]
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
            height: height_new * 0.04,
            width: double.infinity,
            decoration: BoxDecoration(
              color: loailich == 'Lịch hẹn khám' ? primaryColor : secondColor,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8),
                topRight: Radius.circular(8))
            ),
            
            child: Row(
              children: [
                Expanded(
                  flex: 5,
                  child: Text(
                    loailich!,
                    style: TextStyle(color: Colors.white, fontSize: 14),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Text(
                    thoigiandat!,
                    style: TextStyle(color: Colors.white, fontSize: 14),
                  ),
                )
              ],
            ),
          ),

          Container(
            height: height_new * 0.22,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(8),
                bottomRight: Radius.circular(8))
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  RowTextItem(lable: 'Bệnh nhân', content: tenbenhNhan!),
                  RowTextItem(lable: 'Phòng khám', content: tenphongKham!),
                  RowTextItem(lable: 'Lý do khám', content: lydo!),
                  RowTextItem(lable: 'Thời gian', content: thoigianhen!)
                ],
              ),
              // child: Row(
              //   children: [
              //     Column(
              //       crossAxisAlignment: CrossAxisAlignment.start,
              //       mainAxisAlignment: MainAxisAlignment.spaceAround,
              //       children: [
              //         Text('Bệnh nhân'),
              //         Text('Phòng khám'),
              //         Text('Lý do khám'),
              //         Text('Thời gian')
              //       ],
              //     ),
              //     SizedBox(width: width_new*0.05),
              //     Expanded(
              //       child: Column(
              //         crossAxisAlignment: CrossAxisAlignment.start,
              //         mainAxisAlignment: MainAxisAlignment.spaceAround,
              //         children: [
              //           Text(tenbenhNhan!),
              //           Text(tenphongKham!),
              //           Text(lydo!),
              //           Text(thoigianhen!)
              //       ],
              //       )
              //     )                  
              //   ],
              // )
            )
          )
        ],
      ),
    )
    );
  }
}
