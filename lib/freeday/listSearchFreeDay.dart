import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'package:test_item/freeday/add_freeday.dart';
// import 'package:test_item/item/box_search.dart';

import '../item/box_search.dart';
import '../item/custom_button.dart';
import '../item/itemInputSearch.dart';
import 'add_freeday.dart';

class ListSearchFreeDay extends StatefulWidget {
  const ListSearchFreeDay(
      {required this.addFreeDay, required this.SearchFreeDay, super.key});
  final Function(String) addFreeDay;
  final Function(List<String>) SearchFreeDay;
  @override
  State<ListSearchFreeDay> createState() => _ListSearchItemState();
}

class _ListSearchItemState extends State<ListSearchFreeDay> {
  final _focusLoai = FocusNode();
  final _focusNgayNghi = FocusNode();

  final _textEdittingLoai = TextEditingController();
  final _textEdittingCNgayNghi = TextEditingController();
  @override
  void initState() {
    super.initState();
    _focusLoai.addListener(_onFocusChange);
    _focusNgayNghi.addListener(_onFocusChange);
  }

  List<String> result = ['null', 'null'];
  String dataFromChild = '';
  void _onFocusChange() {
    setState(() {
      result[0] = _textEdittingLoai.text;
      result[1] = _textEdittingCNgayNghi.text;
    });
  }

  @override
  void dispose() {
    super.dispose();
    _focusLoai.dispose();
    _focusNgayNghi.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Container(
        margin: EdgeInsets.fromLTRB(20, 10, 20, 0),
        decoration: BoxDecorationSearch,
        child: Column(
          children: [
            Row(
              children: <Widget>[
                Expanded(
                    child: ItemSearchInput(
                  title: 'Loại',
                  focusNode: _focusLoai,
                  textEditingController: _textEdittingLoai,
                )),
                Expanded(
                    child: ItemSearchInput(
                  title: 'Ngày nghỉ',
                  focusNode: _focusNgayNghi,
                  textEditingController: _textEdittingCNgayNghi,
                )),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                CustomButton(
                    text: 'Thêm mới',
                    icon: Icons.add_circle_outline_outlined,
                    onClick: () {
                      Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => AddFreeDay()))
                          .then((value) {
                        widget.addFreeDay(value);
                      });
                    }),
                CustomButton(
                    text: 'Tìm kiếm',
                    icon: Icons.search_sharp,
                    onClick: () {
                      widget.SearchFreeDay(result);
                    })
              ],
            ),
            SizedBox(
              height: 10,
            )
          ],
        ),
      ),
    );
  }
}
