import 'package:flutter/material.dart';
import 'package:product_layout_app/freeday/choosing_freeday.dart';
// import 'package:test_item/api/callapi.dart';
// import 'package:test_item/freeday/choosing_freeday.dart';
// import 'package:test_item/item/line_dropbutton.dart';

import '../api/callapi.dart';
import '../item/DateButton.dart';
import '../item/elevated_button.dart';
import '../item/line_dropbutton.dart';
import '../item/line_text.dart';
import '../model/freeday.dart';

class AddFreeDay extends StatefulWidget {
  const AddFreeDay({super.key});
  @override
  State<AddFreeDay> createState() => _AddFreeDayState();
}

class _AddFreeDayState extends State<AddFreeDay> {
  String type = '';
  String day = '';
  bool isMorning = false;
  bool isAfternoon = false;
  bool isRemote = false;
  bool isCSYT = false;
  String rangeHour = '';
  DateTimeRange selectedDates =
      DateTimeRange(start: DateTime.now(), end: DateTime.now());
  void _showDateRangePicker(BuildContext context) async {
    final DateTimeRange? result = await showDateRangePicker(
      context: context,
      firstDate: DateTime(2023),
      lastDate: DateTime(2024),
      currentDate: DateTime.now(),
      saveText: 'OK',
      builder: (context, child) {
        return Theme(
          data: Theme.of(context).copyWith(
            colorScheme: const ColorScheme.light(
              primary: Colors.blue,
              onPrimary: Colors.black,
              onSurface: Colors.black,
              background: Colors.blue,
              brightness: Brightness.light,
            ),
            textButtonTheme: TextButtonThemeData(
              style: TextButton.styleFrom(
                primary: Colors.black,
              ),
            ),
          ),
          child: child!,
        );
      },
    );
    if (result != null) {
      print(result.start.toString());
      List<String> beginDate =
          result.start.toString().substring(0, 10).split('-');
      List<String> endDate = result.end.toString().substring(0, 10).split('-');
      setState(() {
        rangeHour = beginDate[2] +
            '/' +
            beginDate[1] +
            '/' +
            beginDate[0] +
            ' - ' +
            endDate[2] +
            '/' +
            endDate[1] +
            '/' +
            endDate[0];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: Color(0xFF6F9BD4),
        title: const Text(
          'Thêm ngày nghỉ',
          style: TextStyle(color: Colors.white, fontSize: 16),
        ),
      ),
      body: Column(
        children: [
          Expanded(
              flex: 8,
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.all(15),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 2,
                          blurRadius: 2,
                          offset: Offset(0, 2),
                        ),
                      ],
                    ),
                    child: Column(
                      children: [
                        const SizedBox(
                          height: 5,
                        ),
                        LineInput(
                          title: 'Loại (*)',
                          text: type,
                          flexLeft: 4,
                          flexRight: 6,
                          keyData: 'TypeFreeday',
                          getData: (value) {
                            setState(() {
                              type = value;
                            });
                          },
                        ),
                        Visibility(
                            visible:
                                type == "Ngày cụ thể trong năm" ? false : true,
                            child: LineInput(
                              text: day,
                              title: 'Thứ (*)',
                              flexLeft: 4,
                              flexRight: 6,
                              keyData: 'ThuTrongTuan',
                              getData: (value) {
                                setState(() {
                                  day = value;
                                });
                              },
                            )),
                        Visibility(
                          visible:
                              type == "Ngày cố định hàng tuần" ? false : true,
                          child: Column(
                            children: [
                              const MyWidget(
                                  title: 'Từ ngày - Đến ngày (*)', text: ''),
                              GestureDetector(
                                onTap: () {
                                  _showDateRangePicker(context);
                                },
                                // child: DateButton(),
                                child: Container(
                                  height: 25,
                                  margin:
                                      const EdgeInsets.fromLTRB(10, 10, 10, 0),
                                  alignment: Alignment.centerLeft,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      border: Border.all(
                                          color: Colors.grey, width: 0.5)),
                                  child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Container(
                                          child: Text(rangeHour,
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(fontSize: 12)),
                                        ),
                                        Container(
                                          height: 21,
                                          margin: EdgeInsets.only(right: 10),
                                          child: const Icon(
                                            Icons.calendar_month_outlined,
                                            size: 14,
                                          ),
                                        )
                                      ]),
                                ),
                              ),
                            ],
                          ),
                        ),
                        ChoosingFreeDay(
                          isMorning: isMorning,
                          isAfternoon: isAfternoon,
                          isRemote: isRemote,
                          isCSYT: isCSYT,
                          onClickMorning: (value) {
                            setState(() {
                              isMorning = value;
                            });
                          },
                          onClickAfternoon: (value) {
                            setState(() {
                              isAfternoon = value;
                            });
                          },
                          onClickCSYT: (value) {
                            setState(() {
                              isCSYT = value;
                            });
                          },
                          onClickRemote: (value) {
                            setState(() {
                              isRemote = value;
                            });
                          },
                        ),
                        const SizedBox(
                          height: 15,
                        )
                      ],
                    ),
                  )
                ],
              )),
          Container(
            padding: EdgeInsets.fromLTRB(15, 5, 15, 5),
            child: CustomElevatedButton(
                onClickIcon: () {
                  addFreeday();
                },
                text: 'Lưu',
                icon: Icons.save_alt,
                color: Color(0xFF6F9BD4),
                colorText: Colors.white),
          )
        ],
      ),
    );
  }

  void addFreeday() async {
    String fromDay;
    String toDay;
    String dayy;
    if (type == "Ngày cụ thể trong năm") {
      dayy = '';
      fromDay = rangeHour.substring(0, 10);
      toDay = rangeHour.substring(13, 23);
    } else {
      fromDay = '';
      toDay = '';
      dayy = day;
    }
    if (type == '' ||
        (dayy == '' && fromDay == '' && toDay == '') ||
        (!isMorning && !isAfternoon && !isCSYT && !isRemote)) {
      print('thong tin khong hop le');
    } else {
      FreeDay freeDay = FreeDay(
          type: type == "Ngày cụ thể trong năm" ? "1" : "0",
          thu: day,
          fromDay: fromDay,
          toDay: toDay,
          freeAfternoon: isAfternoon,
          freeCSYT: isCSYT,
          freeMorning: isMorning,
          freeRemote: isRemote,
          id: '1');
      try {
        String url =
            'https://64d48506b592423e46943417.mockapi.io/flutter/api/freeday';
        final result = await BaseAPI().addFreeDay(url, freeDay);
      } catch (e) {
        print('=================\nloi api addFreeday:' + e.toString());
      }
      Navigator.of(context).pop('refresh');
    }
  }
}
