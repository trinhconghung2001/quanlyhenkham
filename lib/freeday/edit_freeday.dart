import 'package:flutter/material.dart';
// import 'package:test_item/api/callapi.dart';
// import 'package:test_item/freeday/choosing_freeday.dart';
// import 'package:test_item/item/elevated_button.dart';
// import 'package:test_item/item/line_dropbutton.dart';

import '../api/callapi.dart';
import '../item/box_decoration_container.dart';
import '../item/elevated_button.dart';
import '../item/line_dropbutton.dart';
import '../item/line_text.dart';
import '../model/freeday.dart';
import 'choosing_freeday.dart';

class EditFreeDay extends StatefulWidget {
  const EditFreeDay({required this.freeDay, super.key});
  final FreeDay freeDay;
  @override
  State<EditFreeDay> createState() => _EditFreeDayState();
}

class _EditFreeDayState extends State<EditFreeDay> {
  late bool type = widget.freeDay.type == '1' ? true : false;
  late bool isMorning = widget.freeDay.freeMorning!;
  late bool isAfternoon = widget.freeDay.freeAfternoon!;
  late bool isRemote = widget.freeDay.freeRemote!;
  late bool isCSYT = widget.freeDay.freeCSYT!;
  late String thu = widget.freeDay.thu.toString();
  late String rangeHour;
  @override
  void initState() {
    super.initState();
    rangeHour = widget.freeDay.fromDay! + ' - ' + widget.freeDay.toDay!;
  }

  void _showDateRangePicker(BuildContext context) async {
    final DateTimeRange? result = await showDateRangePicker(
      context: context,
      firstDate: DateTime(2023),
      lastDate: DateTime(2024),
      currentDate: DateTime.now(),
      saveText: 'OK',
      builder: (context, child) {
        return Theme(
          data: Theme.of(context).copyWith(
            colorScheme: const ColorScheme.light(
              primary: Colors.blue,
              onPrimary: Colors.black,
              onSurface: Colors.black,
              background: Colors.blue,
              brightness: Brightness.light,
            ),
            textButtonTheme: TextButtonThemeData(
              style: TextButton.styleFrom(
                primary: Colors.black,
              ),
            ),
          ),
          child: child!,
        );
      },
    );
    if (result != null) {
      print(result.start.toString());
      List<String> beginDate =
          result.start.toString().substring(0, 10).split('-');
      List<String> endDate = result.end.toString().substring(0, 10).split('-');
      setState(() {
        rangeHour = beginDate[2] +
            '/' +
            beginDate[1] +
            '/' +
            beginDate[0] +
            ' - ' +
            endDate[2] +
            '/' +
            endDate[1] +
            '/' +
            endDate[0];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: Color(0xFF6F9BD4),
        title: const Text(
          'Sửa thông tin ngày nghỉ',
          style: TextStyle(color: Colors.white, fontSize: 16),
        ),
      ),
      body: Scaffold(
        bottomNavigationBar: Container(
          height: 40,
          padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              // CustomElevatedButton(
              //     onClickIcon: () {},
              //     text: 'Xóa',
              //     icon: Icons.delete_outline,
              //     color: Colors.white,
              //     colorText: Color(0xFF6F9BD4)),
              CustomElevatedButton(
                  onClickIcon: () {
                    editFreeDay();
                  },
                  text: 'Lưu',
                  icon: Icons.delete_outline,
                  color: Color(0xFF6F9BD4),
                  colorText: Colors.white),
            ],
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(15),
            decoration: BoxecorationContainer,
            child: Column(
              children: [
                const SizedBox(
                  height: 5,
                ),
                LineInput(
                  title: 'Loại (*)',
                  text: widget.freeDay.type == '1'
                      ? ' Ngày cụ thể trong năm'
                      : ' Ngày cố định hàng tuần',
                  getData: (value) {
                    setState(() {
                      widget.freeDay.type =
                          (value == 'Ngày cụ thể trong năm') ? "1" : '0';
                      type = (value == 'Ngày cụ thể trong năm') ? true : false;
                    });
                  },
                  keyData: 'TypeFreeday',
                  flexLeft: 4,
                  flexRight: 6,
                ),
                Visibility(
                  visible: type ? false : true,
                  child: LineInput(
                    title: 'Thứ (*)',
                    text: thu,
                    getData: (value) {
                      setState(() {
                        thu = value;
                      });
                    },
                    keyData: 'ThuTrongTuan',
                    flexLeft: 4,
                    flexRight: 6,
                  ),
                ),
                Visibility(
                  visible: type ? true : false,
                  child: Column(
                    children: [
                      MyWidget(title: 'Từ ngày - Đến ngày (*)', text: ''),
                      GestureDetector(
                        onTap: () {
                          _showDateRangePicker(context);
                        },
                        child: Container(
                          height: 25,
                          margin: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                          alignment: Alignment.centerLeft,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border:
                                  Border.all(color: Colors.grey, width: 0.5)),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  // width: 125,
                                  child: Text(rangeHour,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(fontSize: 12)),
                                ),
                                Container(
                                  height: 21,
                                  margin: EdgeInsets.only(right: 10),
                                  child: const Icon(
                                    Icons.calendar_month_outlined,
                                    size: 14,
                                  ),
                                )
                              ]),
                        ),
                      ),
                    ],
                  ),
                ),
                ChoosingFreeDay(
                    isMorning: isMorning,
                    isAfternoon: isAfternoon,
                    isRemote: isRemote,
                    isCSYT: isCSYT,
                    onClickMorning: (value) {
                      setState(() {
                        isMorning = value;
                      });
                    },
                    onClickAfternoon: (value) {
                      setState(() {
                        isAfternoon = value;
                      });
                    },
                    onClickRemote: (value) {
                      setState(() {
                        isRemote = value;
                      });
                    },
                    onClickCSYT: (value) {
                      setState(() {
                        isCSYT = value;
                      });
                    }),
                SizedBox(
                  height: 15,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void editFreeDay() async {
    String type = widget.freeDay.type!;
    bool morning = isMorning;
    bool afternoon = isAfternoon;
    bool remote = isRemote;
    bool csyt = isCSYT;
    String fromDay;
    String toDay;
    String idd = '1';
    String thutuan;
    if (type == '1') {
      thu = '';
      fromDay = rangeHour.substring(0, 10);
      toDay = rangeHour.substring(13, 23);
    } else {
      fromDay = '';
      toDay = '';
      thutuan = thu;
    }
    try {
      FreeDay freeDay = FreeDay(
          type: type,
          thu: thu,
          fromDay: fromDay,
          toDay: toDay,
          freeMorning: morning,
          freeAfternoon: afternoon,
          freeRemote: remote,
          freeCSYT: csyt,
          id: idd);
      String url =
          'https://64d48506b592423e46943417.mockapi.io/flutter/api/freeday/';
      final result =
          await BaseAPI().updateStateFreeday(url, widget.freeDay.id!, freeDay);
      Navigator.of(context).pop('refresh');
    } catch (e) {
      print('=====================\nloi goi update' + e.toString());
    }
  }
}
