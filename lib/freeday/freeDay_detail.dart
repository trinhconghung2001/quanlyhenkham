import 'package:flutter/material.dart';
// import 'package:test_item/freeday/choosing_freeday.dart';

import '../dialog/delete_dialog.dart';
import '../item/box_decoration_container.dart';
import '../item/elevated_button.dart';
import '../item/line_text.dart';
import '../model/freeday.dart';
import '../vatTu/edit_DVT.dart';
import 'choosing_freeday.dart';
import 'edit_freeday.dart';

class FreeDayDetail extends StatelessWidget {
  const FreeDayDetail({required this.freeDay, super.key});
  final FreeDay freeDay;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: Color(0xFF6F9BD4),
        title: const Text(
          'Chi tiết thông tin ngày nghỉ',
          style: TextStyle(color: Colors.white, fontSize: 16),
        ),
      ),
      body: Scaffold(
        bottomNavigationBar: Container(
            height: 40,
            padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                CustomElevatedButton(
                    onClickIcon: () {
                      showDialog(
                          context: context,
                          builder: (context) {
                            return AlertPopup(
                              id: freeDay.id!,
                              url:
                                  'https://64d48506b592423e46943417.mockapi.io/flutter/api/freeday/',
                            );
                          }).then((value) {
                        Navigator.of(context).pop(value);
                      });
                    },
                    text: 'Xóa',
                    icon: Icons.save_alt,
                    color: Colors.white,
                    colorText: Color(0xFF6F9BD4)),
                CustomElevatedButton(
                    onClickIcon: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  EditFreeDay(freeDay: freeDay))).then((value) {
                                    Navigator.of(context).pop(value);
                                  });
                    },
                    text: 'Sửa',
                    icon: Icons.edit_note_outlined,
                    color: Color(0xFF6F9BD4),
                    colorText: Colors.white),
              ],
            )),
        body: Container(
          margin: EdgeInsets.all(15),
          decoration: BoxecorationContainer,
          child: SingleChildScrollView(
            child: Column(
              children: [
                const MyWidget(
                    title: 'Loại (*)', text: 'Ngày cụ thể trong năm'),
                MyWidget(title: 'Từ ngày (*)', text: freeDay.fromDay!),
                MyWidget(title: 'Đến ngày (*)', text: freeDay.toDay!),
                ChoosingFreeDay(
                  isMorning: freeDay.freeMorning!,
                  isAfternoon: freeDay.freeAfternoon!,
                  isRemote: freeDay.freeRemote!,
                  isCSYT: freeDay.freeCSYT!,
                  onClickMorning: (value) {},
                  onClickAfternoon: (value) {},
                  onClickRemote: (value) {},
                  onClickCSYT: (value) {},
                ),
                const SizedBox(
                  height: 15,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
